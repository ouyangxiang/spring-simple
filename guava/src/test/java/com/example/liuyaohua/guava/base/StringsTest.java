package com.example.liuyaohua.guava.base;

import com.google.common.base.Strings;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2014-10-17 19:24
 *
 * 说明 ...
 */
public class StringsTest {

    @Test
    public void testStrins() {

        // 判断是否为null 或 ""
        boolean result = Strings.isNullOrEmpty(null);

        // 字符串为null则使用“”替代
        String str2 = Strings.nullToEmpty(null);

        String a = "abc aaa xx";
        String b = "abc bbb xx";
        // 公共前缀
        String prefix = Strings.commonPrefix(a, b);
        // 公共后缀
        String suffix = Strings.commonSuffix(a, b);

        // 字符长度填充
        String str3 = Strings.padEnd("a2", 5, '*');
        String str4 = Strings.padStart("a2", 5, '*');

        // 字符串复制
        String str5 = Strings.repeat("a2", 2);

        // 字符串为空值是使用null替代
        String str1 = Strings.emptyToNull("");

    }
}
