package com.example.liuyaohua.core.filter;

import com.example.liuyaohua.model.Token;
import com.example.liuyaohua.model.enums.ErrorCode;
import com.google.common.collect.Lists;
import com.example.liuyaohua.model.Token;
import com.example.liuyaohua.model.enums.ErrorCode;
import com.example.liuyaohua.util.Configuration;
import com.example.liuyaohua.util.FilterUtils;
import com.example.liuyaohua.util.TokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2016-12-20 17:48
 * 说明 登录过滤器
 */
public class LoginFilter implements Filter {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * token加密key（重要）
	 */
	private String TOKEN_KEY = Configuration.getValue("TOKEN_KEY");
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	/**
	 * rsa 私钥
	 */
	//private String RSA_PRIVATE_KEY = Configuration.getValue("RSA_PRIVATE_KEY");

	/**
	 * 是否启动
	 */
	//private boolean enable = false;
	//private String environment = Configuration.getValue("environment");

	// 黑名单 URI列表
	//private final List<String> forbidAccessServletPaths = Lists.newLinkedList();
	// 忽略登录的URI列表
	private final List<String> ignoreLoginServletPaths = Lists.newArrayList();
	// 白名单 对所有过滤器有效
	private final List<String> ignoreCheckServletPaths = Lists.newArrayList();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		//String filterEnable = filterConfig.getInitParameter("filterEnable");
		//enable = Strings.isNullOrEmpty(filterEnable) || Boolean.parseBoolean(filterEnable);
		//enable = !Strings.isNullOrEmpty(environment) && Objects.equals("prd",environment);

		// 加载黑名单
//		String forbidAccessServletPathStr = filterConfig.getInitParameter("forbidAccessServletPaths");
//		forbidAccessServletPaths.addAll(FilterUtils.getUrlFromStr(forbidAccessServletPathStr));

		// 加载白名单
		String ignoreCheckServletPathStr = filterConfig.getInitParameter("ignoreCheckServletPaths");
		ignoreCheckServletPaths.addAll(FilterUtils.getUrlFromStr(ignoreCheckServletPathStr));

		// 加载不需要登录的接口
		String ignoreLoginServletPathStr = filterConfig.getInitParameter("ignoreLoginServletPaths");
		ignoreLoginServletPaths.addAll(FilterUtils.getUrlFromStr(ignoreLoginServletPathStr));
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		String servletPath = req.getServletPath();

		// 判断是否在黑名单中
		/*if (FilterUtils.isInForbidAccessPaths(req, forbidAccessServletPaths)) {
			FilterUtils.resContent(res, ErrorCode.FORBID_ACCESS.getCode(), ErrorCode.FORBID_ACCESS.getMessage());
			return;
		}*/

		// 白名单 对所有过滤器有效
		if (FilterUtils.isIgnoreCheckValid(servletPath, ignoreCheckServletPaths)) {
			chain.doFilter(request, response);
			return;
		}

		/*if (enable && !Strings.isNullOrEmpty(servletPath) && !servletPath.endsWith(".json")) {// 正式环境 & !=".json"
			logger.error("禁止访问,url = {}",servletPath);
			FilterUtils.resContent(res, ErrorCode.FORBID_ACCESS.getCode(), ErrorCode.FORBID_ACCESS.getMessage());
			return;
		}*/

		// 不需要登录的接口
		if (FilterUtils.isIgnoreCheckValid(servletPath, ignoreLoginServletPaths)) {
			chain.doFilter(request, response);
			return;
		}

		try {
			Token token = TokenUtil.getTokenFromRequest(req, TOKEN_KEY);
			if (token == null) {
				logger.warn("未登录，请登录后操作。request_time={}, url={}",sdf.format(new Date(System.currentTimeMillis())), FilterUtils.getUrlInfo(req));
				FilterUtils.resContent(res, ErrorCode.NEED_LOGIN.getCode(), "未登录，请登录后操作!");
				return;
			}
			if (token.getCreateTime() + token.getPeriodOfValidit() < new Date().getTime()) {  // token过期
				logger.warn("登录有效期已过,请重新登录。token={}",token.toString());
				FilterUtils.resContent(res, ErrorCode.NEED_LOGIN.getCode(), "登录有效期已过,请重新登录!");
				return;
			}
			logger.info("请求token = {}",token.toString());
		} catch (Exception e) {
			logger.error("登录出错：errormsg={}", e.getMessage(), e);
			FilterUtils.resContent(res, ErrorCode.NEED_LOGIN.getCode(), e.getMessage());
			return;
		}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}
}