SET MODE MYSQL;

drop table if exists `user_1`;
create table `user_1` (
  `id`     int(10) not null primary key AUTO_INCREMENT ,
  `name`   varchar(20) not null default '' ,
  `status` int(11) not null DEFAULT 0
);