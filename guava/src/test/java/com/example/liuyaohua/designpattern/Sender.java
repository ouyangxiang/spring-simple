package com.example.liuyaohua.designpattern;

/**
 * 作者 yaohua.liu
 * 日期 2017-05-31 17:40
 * 说明 ...
 */
public interface Sender {
	public void Send();
}
