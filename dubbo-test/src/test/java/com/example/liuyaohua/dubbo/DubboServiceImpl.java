package com.example.liuyaohua.dubbo;

import com.example.liuyaohua.service.DubboService;
import org.springframework.stereotype.Service;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-07-26 16:44
 * <p>说明 ...
 */
@Service
public class DubboServiceImpl implements DubboService {
    @Override
    public String sayHello(int id) {
        if(id == 1){
            return "123456";
        }else {
            return "000000";
        }
    }
}
