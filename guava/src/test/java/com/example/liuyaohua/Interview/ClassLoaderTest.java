package com.example.liuyaohua.Interview;

import org.junit.Test;

import java.io.File;
import java.util.StringTokenizer;

/**
 * 作者： yaohua.liu
 * 日期： 2017-07-12
 * 描述： -XX:+TraceClassLoading 查看类加载的信息
 * <p>
 * 1.分类
 * <p>
 * 1.1.Bootstrap ClassLoader(启动类加载器)
 * 加载JAVA_HOME/lib目录下的核心api 或 -Xbootclasspath 选项指定的jar包装入工作,
 * 是用原生代码来实现的, 并不继承自 java.lang.ClassLoader。
 * <p>
 * 1.2.Extension ClassLoader(扩展类加载器)
 * 加载 Java 的扩展库。Java 虚拟机的实现会提供一个扩展库目录
 * 加载JAVA_HOME/lib/ext目录下的jar包或 -Djava.ext.dirs 指定目录下的jar包
 * <p>
 * 1.3.System ClassLoader(系统类加载器)
 * 加载java -classpath/-cp/-Djava.class.path所指的目录下的类与jar包
 * 它根据 Java 应用的类路径（CLASSPATH）来加载 Java 类。一般来说，Java 应用的类都是由它来完成加载的。
 * 可以通过 ClassLoader.getSystemClassLoader()来获取它。
 * <p>
 * 每个classpath以文件名或目录结尾，该文件名或目录取决于将类路径设置成什么：
 * 对于包含.class文件的.zip或.jar文件，路径以.zip或.jar文件名结尾。
 * 对于未命名包中的.class文件，路径以包含.class文件的目录结尾。
 * 对于已命名包中的.class文件，路径以包含root包(完整包名中的第一个包)的目录结尾。
 * <p>
 * 1.4.自定义类加载器
 * 通过继承 java.lang.ClassLoader类的方式实现自己的类加载器，
 * 用户自定义 ClassLoader 可以根据用户的需要定制自己的类加载过程，在运行期进行指定类的动态实时加载。
 */
public class ClassLoaderTest {

	public static void main(String[] args) {
		/*ClassLoader rt = Byte.class.getClassLoader();
		while (rt != null) {
			System.out.println("rt package 加载器（Bootstrap ClassLoader(启动类加载器)）:"+rt.toString());// 没有输出，即等于 null，
			rt = rt.getParent();
		}*/

		ClassLoader loader = ClassLoaderTest.class.getClassLoader();
		while (loader != null) {
			System.out.println("用户 package 加载器（System ClassLoader(系统类加载器)）:" + loader.toString());
			loader = loader.getParent();
		}
		System.out.println(loader);
//		第一个输出的是 ClassLoaderTest类的类加载器, 即系统类加载器。它是 sun.misc.Launcher$AppClassLoader类的实例
//		第二个输出的是扩展类加载器, 是 sun.misc.Launcher$ExtClassLoader类的实例。
//		这里并没有输出引导类加载器, 这是由于JDK 的实现对于父类加载器是引导类加载器的情况, getParent()方法返回 null。
	}

	@Test
	public void testBootstrapClassload() {
		System.out.println("bootstrap classload----------------------");
		final java.lang.String s = System.getProperty("sun.boot.class.path");
		System.out.println(s);
		final File[] path = (s == null) ? new File[0] : getClassPath(s);
		for (File f : path) {
			System.out.println(f);
		}
		System.out.println();
		sun.misc.Launcher launcher = sun.misc.Launcher.getLauncher();
		System.out.println(launcher.getClass().getClassLoader());

	}

	@Test
	public void testExtClassload() {
		System.out.println("ext classload----------------------");
		final java.lang.String s = System.getProperty("java.ext.dirs");//对应路径
		System.out.println(s);

		File[] dirs;
		if (s != null) {
			StringTokenizer st = new StringTokenizer(s, File.pathSeparator);
			int count = st.countTokens();
			dirs = new File[count];
			for (int i = 0; i < count; i++) {
				dirs[i] = new File(st.nextToken());
			}
		} else {
			dirs = new File[0];
		}

		for (File f : dirs) {
			System.out.println(f.getAbsolutePath());
		}
	}

	@Test
	public void testAppClassload() {
		System.out.println("app classload----------------------");
		final java.lang.String s = System.getProperty("java.class.path");
		System.out.println(s);
		final File[] path = (s == null) ? new File[0] : getClassPath(s);
		for (File f : path) {
			System.out.println(f);
		}
	}

	private File[] getClassPath(java.lang.String path) {
		File file = new File(path);
		if (file.isDirectory()) {
			return file.listFiles();
		}
		return new File[]{file};

	}
}
