package com.example.liuyaohua.guava.concurrent;

import com.google.common.base.Throwables;
import com.google.common.util.concurrent.*;
import org.junit.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-26 17:23
 * * 说明
 * 1）传统JDK中的Future通过异步的方式计算返回结果:在多线程运算中可能或者可能在没有结束返回结果，Future是运行中的多线程的一个引用句柄，确保在服务执行返回一个Result。
 * 2）ListenableFuture可以允许你注册回调方法(callbacks)，在运算（多线程执行）完成的时候进行调用,或者在运算（多线程执行）完成后立即执行。这样简单的改进，使得可以明显的支持更多的操作，这样的功能在JDK concurrent中的Future是不支持的。
 */
public class ListenableFutureTest {

    @Test
    public void test() {

        ListeningExecutorService listeningExecutorService = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));
        ListenableFuture listenableFuture = listeningExecutorService.submit(new Callable() {

            public Object call() throws Exception {
                return "call";
            }
        });
        listenableFuture.addListener(new Runnable() {

            public void run() {
                System.out.println("listenableFuture  over !");
            }
        },listeningExecutorService);

        Futures.addCallback(listenableFuture, new FutureCallback() {

            public void onSuccess(Object result) {
                setResult(result);
            }

            public void onFailure(Throwable t) {
                Throwables.propagate(t);
            }
        });
    }

    private void setResult(Object o) {
        String result= String.valueOf(o);
    }
}
