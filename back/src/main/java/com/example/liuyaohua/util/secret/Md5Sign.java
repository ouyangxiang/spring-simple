package com.example.liuyaohua.util.secret;

/**
 * ==============================================================================
 * Copyright (c) 2013 by www.wacai.com, All rights reserved.
 * ==============================================================================
 * This software is the confidential and proprietary information of
 * wacai.com, Inc. ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with wacai.com, Inc.
 * ------------------------------------------------------------------------------
 * File name: Md5Sign.java
 * Author: yinpiao
 * Date: 2013-12-2 上午11:17:31
 * Description:
 * Nothing.
 * Function List:
 * 1. Nothing.
 * History:
 * 1. Nothing.
 * ==============================================================================
 */

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * WEB参数MD5签名
 *
 * @author yinpiao
 */
public class Md5Sign {

	private Map<String, String> keyValues = new HashMap<String, String>();

	public Md5Sign append(String name, String value) {
		if ("sign".equals(name) || "sign_type".equals(name)) {
			return this;
		}
		keyValues.put(name, value);
		return this;
	}

	public String getWebParams() {
		StringBuilder builder = new StringBuilder();
		Set<String> keys = new TreeSet<String>(new Comparator<String>() {

			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
		keys.addAll(keyValues.keySet()); // 替换set集合
		Iterator<String> it = keys.iterator();
		while (it.hasNext()) {
			String name = it.next();
			String value = keyValues.get(name);
			builder.append(EncodeUtils.encodeURL(name)).append("=").append(EncodeUtils.encodeURL(value));
			if (it.hasNext()) {
				builder.append("&");
			}
		}
		if (builder.length() > 0) {
			builder.append("&sign_type=md5&sign=").append(getSign());
		}
		return builder.toString();
	}

	public String getOrigString() {
		StringBuilder builder = new StringBuilder();
		Set<String> keys = new TreeSet<String>(new Comparator<String>() {

			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
		keys.addAll(keyValues.keySet()); // 替换set集合
		Iterator<String> it = keys.iterator();
		while (it.hasNext()) {
			String name = it.next();
			String value = keyValues.get(name);
			builder.append(name).append("=").append(value);
			if (it.hasNext()) {
				builder.append("&");
			}
		}
		return builder.toString();
	}

	public String getSign() {
		return digist(getOrigString());
	}

	public static String digist(String text) {
		String result = "";

		try {
			MessageDigest md = MessageDigest.getInstance("md5");
			md.update(text.getBytes());
			byte[] b = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			result = buf.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 解析签名参数
	 *
	 * @param params 参数
	 * @return 返回Md5签名对象
	 */
	public static Md5Sign parse(String params) {
		Md5Sign sign = new Md5Sign();
		// if(StringUtils.isBlank(params)) {
		// return sign;
		// }
		if (params.equals("")) {
			return sign;
		}
		String[] slices = params.split("&");
		for (String slice : slices) {
			// if(StringUtils.isNotBlank(slice)) {
			if (!slice.equals("")) {
				String[] item = slice.split("=");
				String name = item[0];
				if ("sign".equals(name) || "sign_type".equals(name)) {
					continue;
				}
				if (item.length > 1) {
					sign.append(item[0], item[1]);
				} else {
					sign.append(item[0], "");
				}
			}
		}
		return sign;
	}

	public boolean verify(String sign) {
		// return StringUtils.equalsIgnoreCase(sign, getSign());
		return sign.equalsIgnoreCase(getSign());
	}

	/**
	 * md5加密
	 *
	 * @param plainText 需要加密的字符串
	 * @return
	 */
	public static String md5(String plainText) {
		String md5str = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes("utf-8"));
			byte b[] = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
	   /* 进行加密处理 */

			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			md5str = buf.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return md5str;
	}

	public String toString() {
		return getWebParams();
	}

}
