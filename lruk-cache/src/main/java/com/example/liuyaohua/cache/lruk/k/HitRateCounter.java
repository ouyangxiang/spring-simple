package com.example.liuyaohua.cache.lruk.k;

import com.example.liuyaohua.cache.lruk.lru.StatsCounter;
import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;
import com.example.liuyaohua.cache.lruk.lru.StatsCounter;
import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;

import java.util.concurrent.TimeUnit;


/**
 * 作者 yaohua.liu
 * 日期 2018-03-20 16:19
 * 说明 K命中率统计
 */
public class HitRateCounter implements StatsCounter {
    // 统计频率 1分钟 单位：纳秒
    public final static long recordTimeOut = TimeUnit.SECONDS.toNanos(60);

    public HitRateCounter(long now) {
        this.lastRecordTime = now;
    }

    private volatile long hitCount[] = new long[]{0};
    private volatile long missCount[] = new long[]{0};

    /**
     * 最后记录时间
     */
    private volatile long lastRecordTime = 0;


    @Override
    public synchronized void recordHits(int count) {
        hitCount[0] += count;
    }

    /**
     * @since 11.0
     */
    @Override
    public synchronized void recordMisses(int count) {
        missCount[0] += count;
    }

    @Override
    public void recordLoadSuccess(long loadTime) {

    }

    @Override
    public void recordLoadException(long loadTime) {

    }

    @Override
    public void recordEviction() {

    }

    public long getLastRecordTime() {
        return lastRecordTime;
    }

    public void setLastRecordTime(long lastRecordTime) {
        this.lastRecordTime = lastRecordTime;
    }

    @Override
    public CacheStats snapshot() {
        return new CacheStats(
                hitCount[0],
                missCount[0],
                0,
                0,
                0,
                0);
    }

    @Override
    public void reset(long x) {
        hitCount[0] = x;
        missCount[0] = x;
    }
}
