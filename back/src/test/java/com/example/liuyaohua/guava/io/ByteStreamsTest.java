package com.example.liuyaohua.guava.io;

import com.google.common.base.Charsets;
import com.google.common.io.*;
import org.junit.Test;

import java.io.*;
import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2015-01-08 12:37
 * * 说明 ...
 */
public class ByteStreamsTest {

    @Test
    public void testPlagformAnd007kCheckBillLog() {
        String strs = "stradfaf\nsfasf";
        // 从字符串获取字节写入流
        InputStream is = null;
        Closer closer = Closer.create();
        try {
            // 获取输入流
            is = closer.register(new ByteArrayInputStream(strs.getBytes("utf-8")));
            byte[] bills = ByteStreams.toByteArray(is);
            System.out.println("获取到的文件长度：" + bills.length);
            File temp = new File("007kBillLog");

            // 从指定文件获取一个字节汇
            ByteSink bs = Files.asByteSink(temp);

            OutputStream out = closer.register(bs.openStream());
            if (out != null) {
                out.write(bills);
            }
            List<String> lines = Files.readLines(temp, Charsets.UTF_8);
            System.out.println(lines.size());
            System.out.println(lines.toString());
        } catch (Exception e) {

        } finally {
            try {
                closer.close();
            } catch (IOException e) {
                System.out.println("关闭上传007对帐流失败!");
            }
        }
    }
}
