package com.example.liuyaohua.model;

public enum LogTypeEnum {
	create_user(0, "增加用户"), update_user(1, "更新用户");
	private int code;
	private String des;

	LogTypeEnum(int code, String des) {
		this.code = code;
		this.des = des;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public static LogTypeEnum codeOf(int code) {
		for (LogTypeEnum type : values()) {
			if (type.code == code) {
				return type;
			}
		}

		throw new IllegalArgumentException("Invalid role code: " + code);
	}
}
