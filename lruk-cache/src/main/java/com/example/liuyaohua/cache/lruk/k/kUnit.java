package com.example.liuyaohua.cache.lruk.k;

import java.util.concurrent.locks.ReentrantLock;

/**
 * 作者 yaohua.liu
 * 日期 2018-03-20 16:19
 * 说明 权重 单元
 */
public class kUnit extends ReentrantLock {
    public kUnit() {
    }

    public kUnit(int row, int column) {
        this.row = row;
        this.column = column;
    }

    private int row = 0;
    private int column = 0;
    private static final int UNSET_INT = -1;
    /**
     * 最后访问时间
     */
    private volatile long accessTime = UNSET_INT;
    /**
     * 最后写时间
     */
    private volatile long writeTime = UNSET_INT;
    /**
     * 权重
     */
    private volatile int weight[] = new int[1];

    public long getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(long accessTime) {
        this.accessTime = accessTime;
    }

    public long getWriteTime() {
        return writeTime;
    }

    public void setWriteTime(long writeTime) {
        this.writeTime = writeTime;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getWeight() {
        return weight[0];
    }

    /**
     * 设置权重
     *
     * @param weight 权重
     */
    public void setWeight(int weight) {
        this.weight[0] = weight;
    }

    /**
     * 权重自增1
     */
    public void addWeight() {
        lock();
        try {
            this.weight[0] += 1;
        } finally {
            unlock();
        }
    }
}
