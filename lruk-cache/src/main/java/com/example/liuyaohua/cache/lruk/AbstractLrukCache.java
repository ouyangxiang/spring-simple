package com.example.liuyaohua.cache.lruk;

import com.example.liuyaohua.cache.lruk.k.KLoader;
import com.example.liuyaohua.cache.lruk.k.KRadix;
import com.example.liuyaohua.cache.lruk.k.kUnit;
import com.example.liuyaohua.cache.lruk.lru.CacheBuilder;
import com.example.liuyaohua.cache.lruk.lru.CacheLoader;
import com.example.liuyaohua.cache.lruk.lru.LocalLoadingCache;
import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;
import com.example.liuyaohua.cache.lruk.sync.EnumLrukSyncType;
import com.example.liuyaohua.cache.lruk.sync.LrukSyncClient;
import com.example.liuyaohua.cache.lruk.sync.SyncResult;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 作者 yaohua.liu
 * 日期 2018-03-30 17:30
 * 说明 lru-k缓存基类
 */
public abstract class AbstractLrukCache<K, V> extends ReentrantLock implements LrukCache<K, V> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractLrukCache.class.getName());
    private TimeUnit timeUnit = TimeUnit.SECONDS;
    /**
     * 默认并发级别/线程数
     */
    private static int DEFAULT_CONCURRENCY_LEVEL = 8;
    private final static boolean K_HIT_RATE_SWITCH = false;

    private LocalLoadingCache<K, V> LRU_CACHE = null;

    /**
     * 默认K结构行大小
     */
    private int DEFAULT_K_ROW_SIZE = 1024;
    /**
     * 默认K结构列大小
     */
    private int DEFAULT_K_COLUMN_SIZE = 2048;

    public boolean initCacheIfNecessary() {
        if (LRU_CACHE != null) {
            return true;
        }

        // 只初始化一次
        lock();
        long start = System.currentTimeMillis();
        EnumLrukSyncType type = getSyncType();
        try {
            if (LRU_CACHE != null) {
                return true;
            }

            LRU_CACHE = CacheBuilder.newBuilder()
                    .maximumSize(initMaximumSize())
                    .setkRowSize(initKRowSize())
                    .setkColumnSize(initKColumnSize())
                    .expireAfterAccess(initTimeOut(), initTimeUnit())
                    .concurrencyLevel(initConcurrencyLevel())
                    .build(new CacheLoader<K, V>() {
                               @Override
                               public V load(K key) throws Exception {
                                   return AbstractLrukCache.this.load(key);
                               }
                           },
                            new KLoader() {
                                @Override
                                public int load() {
                                    return getK();
                                }
                            }
                    );
            LOGGER.info("初始化lruk缓存（{}）结束：maximumSize = {}, kRowSize = {}, kColumnSize = {}, timeout = {}, 用时 = {}", type == null ? "" : type.getDesc(), initMaximumSize(), initKRowSize(), initKColumnSize(), initTimeOut(), System.currentTimeMillis() - start);
        } catch (Exception e) {
            LOGGER.error("初始化lruk缓存失败，error msg = {}", e.getMessage(), e);
            return false;
        } finally {
            unlock();
        }
        return true;
    }

    @Override
    public V get(K key) throws Exception {
        this.initCacheIfNecessary();

        V value = LRU_CACHE.get(checkNotNull(key));

        if (this.initKHitRateSwitch()) {//输出K值命中率信息
            this.printKHitRateInfo(key);
        }
        /*if (value != null) {
            return value;
        }
        // 补偿一次，防止第一次获取的数据异常产生null值
        value = this.load(key);
        if (value != null && LRU_CACHE.containsKey(key)) {
            LRU_CACHE.put(key, value);
        }*/
        return value;
    }

    @Override
    public V getUnchecked(K key) {
        this.initCacheIfNecessary();

        V value = LRU_CACHE.getUnchecked(checkNotNull(key));

        if (this.initKHitRateSwitch()) {//输出K值命中率信息
            this.printKHitRateInfo(key);
        }
        /*if (value != null) {
            return value;
        }
        // 补偿一次，防止第一次获取的数据异常产生null值
        value = this.load(key);
        if (value != null && LRU_CACHE.containsKey(key)) {
            LRU_CACHE.put(key, value);
        }*/
        return value;
    }

    @Override
    public V get(K key, boolean fromLocalCache) throws Exception {
        if (fromLocalCache) {
            return this.get(checkNotNull(key));
        }
        return this.load(checkNotNull(key));
    }

    @Override
    public ImmutableMap<K, V> getAll(Iterable<? extends K> keys) throws Exception {
        this.initCacheIfNecessary();
        return LRU_CACHE.getAll(checkNotNull(keys));
    }

    @Override
    public V replace(K key, V value) {
        if (key == null || value == null) {
            return null;
        }
        if (!this.containsKey(key)) {
            return null;
        }
        V v = this.getUnchecked(key);
        LRU_CACHE.put(key, value);
        return v;
    }

    @Override
    public boolean containsKey(K key) {
        if (key == null) {
            return false;
        }
        this.initCacheIfNecessary();
        return LRU_CACHE.containsKey(key);
    }

    @Override
    public V remove(K key) {
        if (key == null) {
            return null;
        }
        this.initCacheIfNecessary();
        if (!this.containsKey(key)) {
            return null;
        }
        return LRU_CACHE.remove(key);
    }

    @Override
    public V reload(K key) {
        this.initCacheIfNecessary();
        if (!this.containsKey(key)) {
            return null;
        }
        V v = load(key);
        if (v == null) {
            return v;
        }
        return this.replace(key, v);
    }

    @Override
    public void put(K key, V value) {
        this.initCacheIfNecessary();
        LRU_CACHE.put(checkNotNull(key), value);
    }

    @Override
    public long size() {
        initCacheIfNecessary();
        return LRU_CACHE.size();
    }

    /**
     * 向address(可能有多个url)中的url依次发送请求，统计返回数据
     *
     * @param key 需要同步的key
     * @return SyncResult
     */
    public SyncResult syncLrukCache(K key) {
        return LrukSyncClient.syncLrukCache(getSyncAddressStr(), (Long) key, getSyncType());
    }

    /**
     * 同步url(多个时以逗号分隔)，如 xxx.com:8080,yyy.com:8080
     */
    protected abstract String getSyncAddressStr();

    /**
     * 同步类型 {@link EnumLrukSyncType}
     */
    protected abstract EnumLrukSyncType getSyncType();

    /**
     * k阀值命中率状态统计，如果最后一次记录时间超时则重置该时间
     *
     * @return CacheStats
     */
    protected CacheStats kHitRateStats() {
        this.initCacheIfNecessary();
        return LRU_CACHE.kHitRateStats();
    }

    private void printKHitRateInfo(K key) {
        CacheStats stats = this.kHitRateStats();
        if (stats == null) {
            return;
        }
        KRadix kRadix = LRU_CACHE.getKRadix();
        kUnit kUnit = kRadix.getWeightByCode(LRU_CACHE.hash(key));
        EnumLrukSyncType type = getSyncType();
        LOGGER.info("类型 = {}, key = {}, k = {}, 命中个数：{}, 未命中个数：{}, 命中率：{}", type == null ? "" : type.getDesc(), (key == null ? "" : key.toString()), kUnit == null ? 0 : kUnit.getWeight(), stats.hitCount(), stats.missCount(), stats.hitRate());
    }

    /**
     * 获取K命中率开关，如果开关打开则日志输出命中率信息
     */
    protected boolean initKHitRateSwitch() {
        return K_HIT_RATE_SWITCH;
    }

    /**
     * 初始化K结构行大小
     */
    protected int initKRowSize() {
        return DEFAULT_K_ROW_SIZE;
    }

    /**
     * 初始化K结构列大小
     */
    protected int initKColumnSize() {
        return DEFAULT_K_COLUMN_SIZE;
    }

    /**
     * 时间单元类型，该方法可覆盖
     *
     * @see TimeUnit
     */
    protected TimeUnit initTimeUnit() {
        return timeUnit;
    }

    /**
     * 初始化并发级别/线程数，该方法可覆盖
     */
    protected int initConcurrencyLevel() {
        return DEFAULT_CONCURRENCY_LEVEL;
    }
}
