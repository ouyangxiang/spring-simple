package com.example.liuyaohua.core.filter;

import com.example.liuyaohua.util.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 作者 yaohua.liu
 * 日期 2016-10-13 11:48
 * 说明 为所有的http请求提供跨域支持
 */
public class CrossDomainFilter implements Filter {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private String CHANNEL_URL = Configuration.getValue("CHANNEL_URL");

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		//start 跨域支持
		HttpServletResponse res = (HttpServletResponse) response;
		HttpServletRequest req = (HttpServletRequest) request;
		String origin = "*";
		/*try{
			origin = req.getHeader("Origin");
			if (Strings.isNullOrEmpty(origin) || !origin.contains(CHANNEL_URL)) {
				logger.warn("request url = {}",FilterUtils.getUrlInfo(req));
				origin = "*";
			}
		}catch (Exception e){
			logger.warn("request url = {}",FilterUtils.getUrlInfo(req));
			origin = "*";
		}*/

		res.setHeader("Access-Control-Allow-Origin", origin);
		res.setHeader("Access-Control-Allow-Credentials", "true");
		res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		res.setHeader("Access-Control-Max-Age", "3600");
		res.setHeader("Access-Control-Allow-Headers", "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With, X-ACCESS-TOKEN-LL");

		chain.doFilter(req, res);
	}

	@Override
	public void destroy() {

	}
}
