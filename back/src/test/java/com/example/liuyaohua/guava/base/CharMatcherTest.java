package com.example.liuyaohua.guava.base;

import com.google.common.base.CharMatcher;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2014-10-20 17:42
 *
 * 说明 ...
 */
public class CharMatcherTest {

    @Test
    public void testCharMatcher() {
        String str = "FirstName LastName   0\t123=456789\ts.jpg";
        //  整数的个数
        int countIn=CharMatcher.DIGIT.countIn(str);
        // 字母的个数
        int countCh=CharMatcher.JAVA_LETTER.countIn(str);
        // 去除两端的空格，并把中间的连续空格替换成单个空格
        String str1=CharMatcher.WHITESPACE.trimAndCollapseFrom(str, ' ');
        // 用*号替换所有数字
        String str2=CharMatcher.JAVA_DIGIT.replaceFrom(str, "*");
        //  只保留数字和小写字母
        String str3=CharMatcher.JAVA_DIGIT.or(CharMatcher.JAVA_LOWER_CASE).retainFrom(str);
        // 保留数字
        String str4=CharMatcher.DIGIT.retainFrom(str);
        // 移除数字
        String str5=CharMatcher.DIGIT.removeFrom(str);
        // 保留字符
        String str6=CharMatcher.JAVA_LETTER.retainFrom(str);
        // 保留字符与数字
        String str7=CharMatcher.JAVA_LETTER_OR_DIGIT.retainFrom(str);
        // 枚举匹配字符。如CharMatcher.anyOf(“aeiou”)匹配小写英语元音
        String str13=CharMatcher.anyOf("aeiou").retainFrom(str);
        // 保留大写字母
        String str14=CharMatcher.JAVA_UPPER_CASE.retainFrom(str);
        // 保留小写字母
        String str15=CharMatcher.JAVA_LOWER_CASE.retainFrom(str);
        // 获取所有单字节长度的符号
        String str16=CharMatcher.SINGLE_WIDTH.retainFrom(str);
        String str17=CharMatcher.JAVA_ISO_CONTROL.retainFrom(str);
        //  删除首部匹配到的字符
        String str18=CharMatcher.anyOf("First Name").trimLeadingFrom(str);
        //  删除首部匹配到的字符
        String str19=CharMatcher.anyOf("FirstName").trimLeadingFrom(str);
        // 删除尾部匹配到的字符
        String str20=CharMatcher.anyOf("s.jpg").trimTrailingFrom(str);
        // 删除首尾匹配到的字符
        String str21=CharMatcher.anyOf("First Name").or(CharMatcher.anyOf("s.jpg")).trimFrom(str);

        String str8=CharMatcher.inRange('a', 'z').or(CharMatcher.inRange('A', 'Z')).or(CharMatcher.inRange('0', '9')).retainFrom(str);
        String str9=CharMatcher.inRange('a', 'z').and(CharMatcher.inRange('A', 'Z')).or(CharMatcher.inRange('0', '9')).retainFrom(str);
        String str10=CharMatcher.inRange('a', 'z').and(CharMatcher.inRange('A', 'Z')).and(CharMatcher.inRange('0', '9')).retainFrom(str);
    }
}
