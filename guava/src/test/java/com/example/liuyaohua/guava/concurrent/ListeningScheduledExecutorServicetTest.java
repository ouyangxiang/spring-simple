package com.example.liuyaohua.guava.concurrent;

import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-27 16:15
 * * 说明 ...
 */
public class ListeningScheduledExecutorServicetTest {

    @Test
    public void testListeningScheduledExecutorService() throws InterruptedException {
        ListeningScheduledExecutorService listeningScheduledExecutorService = MoreExecutors.listeningDecorator(Executors.newScheduledThreadPool(2));
        listeningScheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("run...");
                }
            }
        }, 1, 2, TimeUnit.SECONDS);

        Thread.sleep(1000);
        listeningScheduledExecutorService.shutdown();
    }
}
