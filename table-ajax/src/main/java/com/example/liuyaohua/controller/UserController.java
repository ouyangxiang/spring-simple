package com.example.liuyaohua.controller;

import com.alibaba.fastjson.JSON;
import com.example.liuyaohua.model.Search;
import com.example.liuyaohua.model.SearchResult;
import com.example.liuyaohua.model.vo.APIResponse;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2017-09-19 20:01
 * <p>说明 用户信息
 */
@Controller
@RequestMapping("/user")
public class UserController extends AbstractController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(value = "/queryCheatOrder", method = RequestMethod.POST)
    @ResponseBody
    public APIResponse<List<SearchResult>> queryCheatOrder(
            @RequestBody Search search,
            HttpServletResponse response) {
        logger.info("传入参数 = {}", JSON.toJSONString(search));

        SearchResult result = new SearchResult();
        result.setHotelName("测试酒店");
        result.setUserId("123456");
        result.setOrderNo("abc");
        result.setHotelSeq("13143rqw34523");
        result.setCellid("123");
        result.setCellCity("北京");
        result.setGpsCity("北京");
        result.setUserPhone("15210108091");
        result.setDistance("1200m");
        result.setCreateTime("2018-08-24");

        List<SearchResult> list = Lists.newArrayList();
        list.add(result);
        list.add(result);
        return APIResponse.returnSuccess(list);
    }
}