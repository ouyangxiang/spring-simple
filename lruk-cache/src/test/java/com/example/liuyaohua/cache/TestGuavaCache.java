package com.example.liuyaohua.cache;

import com.example.liuyaohua.cache.guava.AbstractGuavaCache;
import com.example.liuyaohua.cache.guava.GuavaCache;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-06-25 22:39
 * <p>说明 ...
 */
public class TestGuavaCache extends AbstractGuavaCache<Integer,String> implements GuavaCache<Integer,String> {
    @Override
    public String loadData(Integer key) {
        if(key == null){
            return null;
        }
        if(key == 0){
            return null;
        }
        if(key == 1){
            return null;
        }
        return "b";
    }

    @Override
    public long initMaximumSize() {
        return 20;
    }
}
