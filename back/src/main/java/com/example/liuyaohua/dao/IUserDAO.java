package com.example.liuyaohua.dao;

import com.example.liuyaohua.model.User;
import com.example.liuyaohua.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserDAO {
	void addUser(User user) throws Exception;
	User selectUser(String name) throws Exception;
	void updateUser(User user) throws Exception;
}
