package com.example.liuyaohua.cache.lruk.k;

/**
 * 作者 yaohua.liu
 * 日期 2018-04-02 15:53
 * 说明 动态获取K值
 */
public abstract class KLoader {
    /**
     * 获取K结构阀值
     * @return int
     */
    public abstract int load();
}
