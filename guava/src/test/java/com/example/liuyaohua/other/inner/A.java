package com.example.liuyaohua.other.inner;

/**
 * 作者 yaohua.liu
 * 日期 2015-09-07 15:02
 *
 * 说明 A、继承式的匿名内部类
 */

public class A {
    public void drive(){
        System.out.println("Driving a car!");
    }
    public static void main(String[] args) {
        A a = new A(){
            public void drive() {
                System.out.println("Driving another car!");
            }
        };
        a.drive();
    }
}
