package com.example.liuyaohua;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.junit.Test;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-03-16 19:16
 * <p>说明 ...
 */
public class EfficientCounter {

    private static LoadingCache<String, MutableInteger> REGION_INFO = null;

    private static final int CACHE_KEY_SIZE = 1000;

    private static final int CACHE_TIMEOUNT = 1;

    private void initCacheIfNecessary() {
        if (REGION_INFO == null) {
            REGION_INFO = CacheBuilder
                    .newBuilder()
                    .maximumSize(CACHE_KEY_SIZE)
                    .expireAfterWrite(CACHE_TIMEOUNT, TimeUnit.MINUTES)
                    .build(new CacheLoader<String, MutableInteger>() {
                        @Override
                        public MutableInteger load(String k) {
                            return new MutableInteger(1);
                        }
                    });
        }
    }
    int NUM_ITERATIONS = 10000;
    String[] sArr = getArrayString();
    @Test
    public void test(){
        String s = "one two three two three three";
//        String[] sArr = s.split(" ");


        long startTime = 0;
        long endTime = 0;
        long duration = 0;

        // naive approach
        startTime = System.nanoTime();
        HashMap<String, Integer> counter1 = new HashMap<String, Integer>();

        for (int i = 0; i < NUM_ITERATIONS; i++)
            for (String a : sArr) {
                if (counter1.containsKey(a)) {
                    int oldValue = counter1.get(a);
                    counter1.put(a, oldValue + 1);
                } else {
                    counter1.put(a, 1);
                }
            }

        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Naive Approach :  " + duration);

        // better approach
        startTime = System.nanoTime();
        HashMap<String, MutableInteger> newCounter = new HashMap<String, MutableInteger>();

        for (int i = 0; i < NUM_ITERATIONS; i++)
            for (String a : sArr) {
                if (newCounter.containsKey(a)) {
                    MutableInteger oldValue = newCounter.get(a);
                    oldValue.set(oldValue.get() + 1);
                } else {
                    newCounter.put(a, new MutableInteger(1));
                }
            }

        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Better Approach:  " + duration);

        // efficient approach
        startTime = System.nanoTime();

        HashMap<String, int[]> intCounter2 = new HashMap<String, int[]>();
        for (int i = 0; i < NUM_ITERATIONS; i++) {
            for (String a : sArr) {
                int[] valueWrapper = intCounter2.get(a);

                if (valueWrapper == null) {
                    intCounter2.put(a, new int[]{1});
                } else {
                    valueWrapper[0]++;
                }
            }
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("Efficient Approach:  " + duration);

        startTime = System.nanoTime();
        initCacheIfNecessary();;
        for (int i = 0; i < NUM_ITERATIONS; i++) {
            for (String a : sArr) {
                MutableInteger valueWrapper = null;
                try {
                    valueWrapper = REGION_INFO.get(a);
                } catch (Exception e) {

                }

                if (valueWrapper == null) {
                    REGION_INFO.put(a, new MutableInteger());
                } else {
                    valueWrapper.add();
                }
            }
        }

        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("guava Approach:  " + duration);
        for (String a : sArr) {
            MutableInteger valueWrapper = null;
            try {
                valueWrapper = REGION_INFO.get(a);
            } catch (Exception e) {

            }

            if (valueWrapper != null) {
                System.out.println(a+":"+valueWrapper.getValue());
            }
        }
    }

    private String[] getArrayString(){
        String[] ss= new String[50];
        for (int i = 0; i < 50; i++) {
            ss[i] = new String("a"+i);
        }
        System.out.println("生成字符串数组完成");
        return ss;
    }

    class MutableInteger{
        public MutableInteger(){}
        public MutableInteger(int value){
            this.value = value;
        }
        String key = "";
        int value = 1;

        public int get(){
            return value;
        }
        public void add(){
            ++this.value;
        }

        public void set(int value){
            this.value = value;
        }
        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
}
