package com.example.liuyaohua.other.annotation;

/**
 * 作者 yaohua.liu
 * 日期 2015-06-03 11:52
 * 包名 annotation
 * 说明 ...
 */

import org.junit.Test;

import java.lang.annotation.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AnnotionOtherTest {

    /**
     * 说明 要使用SayHiAnnotation的元素所在类
     * 由于我们定义了只有方法才能使用我们的注解，我们就使用多个方法来进行测试
     */
    public class SayHiOperate {

        // 普通的方法
        public void SayHiDefault(String name) {
            System.out.println("Hi, " + name);
        }

        // 使用注解并传入参数的方法
        @SayHiAnnotation(paramValue = "Jack")
        public void SayHiAnnotation(String name) {
            System.out.println("Hi, " + name);
        }

        // 使用注解并使用默认参数的方法
        @SayHiAnnotation
        public void SayHiAnnotationDefault(String name) {
            System.out.println("Hi, " + name);
        }
    }

    /**
     * 自定义注解，用来配置方法
     */
    @Retention(RetentionPolicy.RUNTIME) // 表示注解在运行时依然存在
    @Target(ElementType.METHOD) // 表示注解可以被使用于方法上
    public @interface SayHiAnnotation {
        String paramValue() default "johness"; // 表示我的注解需要一个参数 名为"paramValue" 默认值为"johness"
    }

    @Test
    public void annotionTest()
            throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException {

        SayHiOperate element = new SayHiOperate(); // 初始化一个实例，用于方法调用
        Method[] methods = SayHiOperate.class.getDeclaredMethods(); // 获得所有方法

        for (Method method : methods) {
            System.out.println("this method is :"+method.getName());
            SayHiAnnotation annotation = method.getAnnotation(SayHiAnnotation.class);
            if (annotation != null) { // 检测是否使用了我们的注解
                method.invoke(element, annotation.paramValue()); // 如果使用了我们的注解，我们就把注解里的"paramValue"参数值作为方法参数来调用方法
            } else {
                method.invoke(element, "Rose"); // 如果没有使用我们的注解，我们就需要使用普通的方式来调用方法了
            }
        }
    }
}
