package com.example.liuyaohua.service;

import com.example.liuyaohua.dao.UserDao;
import com.example.liuyaohua.model.User;
import com.google.common.base.Preconditions;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-09-02 00:05
 * <p>说明 ...
 */
@Service
public class UserService {

    public static final String ERROR_MSG = "this is test exception!";

    @Resource
    private UserDao userDao;

    public int saveUser(User user) {
        return userDao.saveUser(user);
    }

    public User queryUserById(int id) {
        return userDao.queryUserById(id);
    }

    public int updateUserById(User user) {
        Preconditions.checkNotNull(user, "用户信息不能为空！");
        return userDao.updateUserById(user);
    }

    public int deleteUserById(int id) {
        Preconditions.checkArgument(id > 0, "id(%s)必须大于0！", id);
        return userDao.deleteUserById(id);
    }

    public void testException() {
        throw new IllegalStateException(ERROR_MSG);
    }
}
