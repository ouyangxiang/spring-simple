package com.example.liuyaohua.Interview;

import org.junit.Assert;

/**
 * 作者 yaohua.liu
 * 日期 2017-05-31 17:00
 * 说明 ...
 */
public class StringBufferAndBuilderTest {

	public void test(){

		// 线程安全
		StringBuffer stringBuffer = new StringBuffer("");
		stringBuffer.append("aa");
		Assert.assertNull(stringBuffer.toString());

		// 线程不安全
		StringBuilder stringBuilder = new StringBuilder("");
		stringBuilder.append("bb");
		Assert.assertNull(stringBuilder.toString());
	}
}
