package com.example.liuyaohua.handler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.liuyaohua.model.LogTypeEnum;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import com.example.liuyaohua.model.LogTypeEnum;

public class LogTypeHandler implements TypeHandler<LogTypeEnum> {

	@Override
	public LogTypeEnum getResult(ResultSet rs, String cloumnName)
			throws SQLException {
		return LogTypeEnum.codeOf(rs.getInt(cloumnName));
	}

	@Override
	public LogTypeEnum getResult(ResultSet rs, int cloumnIndex) throws SQLException {
		return LogTypeEnum.codeOf(rs.getInt(cloumnIndex));
	}

	@Override
	public LogTypeEnum getResult(CallableStatement cs, int cloumnIndex)
			throws SQLException {
		return LogTypeEnum.codeOf(cs.getInt(cloumnIndex));
	}

	@Override
	public void setParameter(PreparedStatement pre, int i,
			LogTypeEnum parameter, JdbcType arg3) throws SQLException {
		pre.setInt(i,parameter.getCode());
	}

}
