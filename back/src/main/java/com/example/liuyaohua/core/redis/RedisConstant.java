package com.example.liuyaohua.core.redis;




public class RedisConstant {
	/* 缓存生命周期等级控制 */
	/** 3秒 */
	public final static long EXPIRES_3_SECOND = 3L;
	/** 3分钟 **/
	public final static long EXPIRES_3_MINUTE = 3 * 60L;
	/** 5分钟 **/
	public final static long EXPIRES_5_MINUTE = 5 * 60L;
	/** 1小时 **/
	public final static long EXPIRES_ONE_HOUR = 60 * 60L;
	/** 2小时 **/
	public final static long EXPIRES_TWO_HOUR = 60 * 60L * 2;
	/** 1天 **/
	public final static long EXPIRES_ONE_DAY = 60 * 60 * 24L;
	/** 7天 **/
	public final static long EXPIRES_SEVEN_DAY = 60 * 60 * 24 * 7L;
	/** 30天 **/
	public final static long EXPIRES_THIRTY_DAY = 60 * 60 * 24 * 30L;


	/* 分页条数控制 */
	/** 分页指数 1级 每页3条 */
	public final static int PAGE_SIZE_LEVEL_1 = 3;
	/** 分页指数 2级 每页5条 */
	public final static int PAGE_SIZE_LEVEL_2 = 5;
	/** 分页指数 3级 每页10条 */
	public final static int PAGE_SIZE_LEVEL_3 = 10;
	/** 分页指数 4级 每页15条 */
	public final static int PAGE_SIZE_LEVEL_4 = 15;
	/** 分页指数 5级 每页20条 */
	public final static int PAGE_SIZE_LEVEL_5 = 20;
	/** 分页指数 6级 每页30条 */
	public final static int PAGE_SIZE_LEVEL_6 = 30;
	/** 分页指数 7级 每页50条 */
	public final static int PAGE_SIZE_LEVEL_7 = 50;
	
	private static String PREFIX = "KNAS_";


	public static String CITY_DICTIONARY_LIST = PREFIX + "cityDictionaryBeanList";
	
	public static String API_CAN_NOT_ACCESS = PREFIX + "apiCanNotAccess";
}
