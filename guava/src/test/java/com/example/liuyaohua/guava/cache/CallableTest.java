package com.example.liuyaohua.guava.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import org.junit.Test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-25 16:32
 * 说明 ...
 */
public class CallableTest {

    private static Cache<Object, Object> cacheFormCallable = null;

    @Test
    public void testCallableCache() throws Exception{
        cacheFormCallable=callableCached();
        String peida="peida";
        Object result=getKey(peida);
        System.out.println("peida=" + result);
        result=getKey(peida);
        System.out.println("peida="+result);
        result=getKey(peida);
        System.out.println("peida="+result);
    }

    /**
     * 对需要延迟处理的可以采用这个机制；(泛型的方式封装)
     *
     * @param <K>
     * @param <V>
     * @return V
     * @throws Exception
     */
    public static <K,V> Cache<K , V> callableCached() throws Exception {

        Cache<K, V> cache = CacheBuilder
                .newBuilder()// 新建一个Cache
                //.weakKeys()// 弱引用key
                .softValues()
                .maximumSize(10000)// 最大的缓存实体数量
                .expireAfterWrite(10, TimeUnit.MINUTES)// 实体创建后或访问后，多长时间内自动移除
                .expireAfterAccess(10, TimeUnit.DAYS)// 实体创建或替换后，多长时间内自动移除
                .removalListener(// 添加Entry移除监听器
                        new RemovalListener<K, V>() {
                            @Override
                            public void onRemoval(RemovalNotification<K, V> rn) {
                                System.out.println(rn.getKey() + " 被移除");
                            }
                        }
                )
                .build();
        return cache;
    }

    private Object getKey(final Object userName) {
        try {
            // Callable只有在缓存值不存在时，才会调用
            return cacheFormCallable.get(userName, new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    System.out.println(userName+" from db");
                    return userName;
                }
            });
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }
}
