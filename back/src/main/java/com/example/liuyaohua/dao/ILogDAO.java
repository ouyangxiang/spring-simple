package com.example.liuyaohua.dao;

import com.example.liuyaohua.model.Log;
import com.example.liuyaohua.util.Page;
import com.example.liuyaohua.model.Log;
import com.example.liuyaohua.util.Page;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ILogDAO {
	void insertLog(Log logger);
	List<Log> selectUserLogByPage(Page page);
}
