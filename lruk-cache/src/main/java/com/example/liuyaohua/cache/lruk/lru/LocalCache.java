/*
 * Copyright (C) 2009 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.example.liuyaohua.cache.lruk.lru;

import com.example.liuyaohua.cache.lruk.lru.entry.EntryFactory;
import com.example.liuyaohua.cache.lruk.lru.entry.NullEntry;
import com.example.liuyaohua.cache.lruk.lru.entry.ReferenceEntry;
import com.google.common.base.Equivalence;
import com.google.common.base.Stopwatch;
import com.google.common.collect.*;
import com.google.common.primitives.Ints;
import com.google.common.util.concurrent.ExecutionError;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.example.liuyaohua.cache.lruk.base.Ticker;
import com.example.liuyaohua.cache.lruk.k.HitRateCounter;
import com.example.liuyaohua.cache.lruk.k.KLoader;
import com.example.liuyaohua.cache.lruk.k.KRadix;
import com.example.liuyaohua.cache.lruk.k.kUnit;
import com.example.liuyaohua.cache.lruk.lru.entry.EntryFactory;
import com.example.liuyaohua.cache.lruk.lru.entry.NullEntry;
import com.example.liuyaohua.cache.lruk.lru.entry.ReferenceEntry;
import com.example.liuyaohua.cache.lruk.lru.reference.ValueReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.ReferenceQueue;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.example.liuyaohua.cache.lruk.lru.CacheBuilder.UNSET_INT;
import static java.util.concurrent.TimeUnit.NANOSECONDS;


public class LocalCache<K, V> extends AbstractMap<K, V> implements ConcurrentMap<K, V> {

    // Constants

    /**
     * The maximum capacity, used if a higher value is implicitly specified by either of the
     * constructors with arguments. MUST be a power of two {@code <= 1<<30} to ensure that entries are
     * indexable using ints.
     */
    public static final int MAXIMUM_CAPACITY = 1 << 30;

    /**
     * The maximum number of segments to allow; used to bound constructor arguments.
     */
    public static final int MAX_SEGMENTS = 1 << 16; // slightly conservative

    /**
     * Number of (unsynchronized) retries in the containsValue method.
     */
    static final int CONTAINS_VALUE_RETRIES = 3;

    /**
     * Number of cache access operations that can be buffered per segment before the cache's recency
     * ordering information is updated. This is used to avoid lock contention by recording a memento
     * of reads and delaying a lock acquisition until the threshold is crossed or a mutation occurs.
     *
     * <p>This must be a (2^n)-1 as it is used as a mask.
     */
    public static final int DRAIN_THRESHOLD = 0x3F;

    /**
     * Maximum number of entries to be drained in a single cleanup run. This applies independently to
     * the cleanup queue and both reference queues.
     */
    // TODO(fry): empirically optimize this
    public static final int DRAIN_MAX = 16;

    // Fields

    public static final Logger logger = LoggerFactory.getLogger(LocalCache.class.getName());

    /**
     * Mask value for indexing into segments. The upper bits of a key's hash code are used to choose the segment.
     * <p>
     * segments's length - 1
     */
    public final int segmentMask;

    /**
     * Shift value for indexing within segments. Helps prevent entries that end up in the same segment
     * from also ending up in the same bucket.
     */
    public final int segmentShift;

    /**
     * The segments, each of which is a specialized hash table.
     */
    public final Segment<K, V>[] segments;
    public final KRadix kRadix;

    /**
     * The concurrency level.
     */
    public final int concurrencyLevel;

    /**
     * Strategy for comparing keys.
     */
    public final Equivalence<Object> keyEquivalence;// 可自定义key的比较方式

    /**
     * Strategy for comparing values.
     */
    public final Equivalence<Object> valueEquivalence;// 可自定义值的比较策略

    /**
     * Strategy for referencing keys.
     */
    public final Strength keyStrength;

    /**
     * Strategy for referencing values.
     */
    public final Strength valueStrength;

    /**
     * The maximum weight of this map. UNSET_INT if there is no maximum.
     */
    public final long maxWeight;

    /**
     * Weigher to weigh cache entries.
     */
    public final Weigher<K, V> weigher;

    /**
     * How long after the last access to an entry the map will retain that entry.
     */
    public final long expireAfterAccessNanos;

    /**
     * How long after the last write to an entry the map will retain that entry.
     */
    public final long expireAfterWriteNanos;

    /**
     * How long after the last write an entry becomes a candidate for refresh.
     */
    public final long refreshNanos;

    /**
     * Entries waiting to be consumed by the removal listener.
     */
    // TODO(fry): define a new type which creates event objects and automates the clear logic
    public final Queue<RemovalNotification<K, V>> removalNotificationQueue;

    /**
     * A listener that is invoked when an entry is removed due to expiration or garbage collection of
     * soft/weak entries.
     */
    public final RemovalListener<K, V> removalListener;

    /**
     * Measures time in a testable way.
     */
    public final Ticker ticker;

    /**
     * Factory used to create new entries.
     */
    public final EntryFactory entryFactory;

    /**
     * Accumulates global cache statistics. Note that there are also per-segments stats counters which
     * must be aggregated to obtain a global stats view.
     */
    public final StatsCounter globalStatsCounter;

    /**
     * The default cache loader to use on loading operations.
     */
    public final CacheLoader<? super K, V> defaultLoader;
    /**
     * 动态获取K值信息
     */
    private KLoader kLoader;
    /**
     * 命中率信息
     */
    public HitRateCounter hitRateCounter;

    /**
     * Creates a new, empty map with the specified strategy, initial capacity and concurrency level.
     */
    public LocalCache(CacheBuilder<? super K, ? super V> builder, CacheLoader<? super K, V> loader, KLoader kLoader) {
        concurrencyLevel = Math.min(builder.getConcurrencyLevel(), MAX_SEGMENTS);

        keyStrength = builder.getKeyStrength();
        valueStrength = builder.getValueStrength();

        keyEquivalence = builder.getKeyEquivalence();
        valueEquivalence = builder.getValueEquivalence();

        maxWeight = builder.getMaximumWeight();
        weigher = builder.getWeigher();
        expireAfterAccessNanos = builder.getExpireAfterAccessNanos();
        expireAfterWriteNanos = builder.getExpireAfterWriteNanos();
        refreshNanos = builder.getRefreshNanos();

        removalListener = builder.getRemovalListener();
        removalNotificationQueue = (removalListener == CacheBuilder.NullListener.INSTANCE) ? LocalCache.<RemovalNotification<K, V>>discardingQueue() : new ConcurrentLinkedQueue<RemovalNotification<K, V>>();

        ticker = builder.getTicker(recordsTime());
        entryFactory = EntryFactory.getFactory(keyStrength, usesAccessEntries(), usesWriteEntries());
        globalStatsCounter = builder.getStatsCounterSupplier().get();
        defaultLoader = loader;
        hitRateCounter = new HitRateCounter(ticker.read());

        int initialCapacity = Math.min(builder.getInitialCapacity(), MAXIMUM_CAPACITY);
        if (evictsBySize() && !customWeigher()) {
            initialCapacity = Math.min(initialCapacity, (int) maxWeight);
        }

        // 初始化K结构
        int kRowSize = builder.getKRowSize();
        int rowSize = 1;
        while (rowSize < kRowSize) {
            rowSize <<= 1;
        }
        int kColumnSize = builder.getkColumnSize();
        int columnSize = 1;
        while (columnSize < kColumnSize) {
            columnSize <<= 1;
        }
        this.kLoader = kLoader;
        kRadix = new KRadix(rowSize, columnSize, ticker, expireAfterAccessNanos, expireAfterWriteNanos);

        // Find the lowest power-of-two segmentCount that exceeds concurrencyLevel, unless
        // maximumSize/Weight is specified in which case ensure that each segment gets at least 10
        // entries. The special casing for size-based eviction is only necessary because that eviction
        // happens per segment instead of globally, so too many segments compared to the maximum size
        // will result in random eviction behavior.
        int segmentShift = 0;
        int segmentCount = 1;
        while (segmentCount < concurrencyLevel && (!evictsBySize() || segmentCount * 20 <= maxWeight)) {
            ++segmentShift;
            segmentCount <<= 1;
        }
        this.segmentShift = 32 - segmentShift;
        segmentMask = segmentCount - 1;

        this.segments = newSegmentArray(segmentCount);

        int segmentCapacity = initialCapacity / segmentCount;
        if (segmentCapacity * segmentCount < initialCapacity) {
            ++segmentCapacity;
        }

        int segmentSize = 1;//每个segment的大小
        while (segmentSize < segmentCapacity) {
            segmentSize <<= 1;
        }

        if (evictsBySize()) {
            // Ensure sum of segment max weights = overall max weights
            long maxSegmentWeight = maxWeight / segmentCount + 1;
            long remainder = maxWeight % segmentCount;
            for (int i = 0; i < this.segments.length; ++i) {
                if (i == remainder) {
                    maxSegmentWeight--;
                }
                this.segments[i] = createSegment(segmentSize, maxSegmentWeight, builder.getStatsCounterSupplier().get());
            }
        } else {
            for (int i = 0; i < this.segments.length; ++i) {
                this.segments[i] = createSegment(segmentSize, CacheBuilder.UNSET_INT, builder.getStatsCounterSupplier().get());
            }
        }
    }

    public KRadix getkRadix() {
        return kRadix;
    }

    public boolean evictsBySize() {
        return maxWeight >= 0;
    }

    public boolean customWeigher() {
        return weigher != CacheBuilder.OneWeigher.INSTANCE;
    }

    public boolean expires() {
        return expiresAfterWrite() || expiresAfterAccess();
    }

    public boolean expiresAfterWrite() {
        return expireAfterWriteNanos > 0;
    }

    public boolean expiresAfterAccess() {
        return expireAfterAccessNanos > 0;
    }

    public boolean refreshes() {
        return refreshNanos > 0;
    }

    public boolean usesAccessQueue() {
        return expiresAfterAccess() || evictsBySize();
    }

    public boolean usesWriteQueue() {
        return expiresAfterWrite();
    }

    public boolean recordsWrite() {
        return expiresAfterWrite() || refreshes();
    }

    public boolean recordsAccess() {
        return expiresAfterAccess();
    }

    public boolean recordsTime() {
        return recordsWrite() || recordsAccess();
    }

    public boolean usesWriteEntries() {
        return usesWriteQueue() || recordsWrite();
    }

    public boolean usesAccessEntries() {
        return usesAccessQueue() || recordsAccess();
    }

    public boolean usesKeyReferences() {
        return keyStrength != Strength.STRONG;
    }

    public boolean usesValueReferences() {
        return valueStrength != Strength.STRONG;
    }

    /**
     * Placeholder. Indicates that the value hasn't been set yet.
     */
    public static final ValueReference<Object, Object> UNSET =
            new ValueReference<Object, Object>() {
                @Override
                public Object get() {
                    return null;
                }

                @Override
                public int getWeight() {
                    return 0;
                }

                @Override
                public ReferenceEntry<Object, Object> getEntry() {
                    return null;
                }

                @Override
                public ValueReference<Object, Object> copyFor(
                        ReferenceQueue<Object> queue,
                        Object value,
                        ReferenceEntry<Object, Object> entry) {
                    return this;
                }

                @Override
                public boolean isLoading() {
                    return false;
                }

                @Override
                public boolean isActive() {
                    return false;
                }

                @Override
                public Object waitForValue() {
                    return null;
                }

                @Override
                public void notifyNewValue(Object newValue) {
                }
            };

    /**
     * Singleton placeholder that indicates a value is being loaded.
     */
    @SuppressWarnings("unchecked") // impl never uses a parameter or returns any non-null value
    public static <K, V> ValueReference<K, V> unset() {
        return (ValueReference<K, V>) UNSET;
    }

    @SuppressWarnings("unchecked") // impl never uses a parameter or returns any non-null value
    public static <K, V> ReferenceEntry<K, V> nullEntry() {
        return (ReferenceEntry<K, V>) NullEntry.INSTANCE;
    }

    public static final Queue<?> DISCARDING_QUEUE =
            new AbstractQueue<Object>() {
                @Override
                public boolean offer(Object o) {
                    return true;
                }

                @Override
                public Object peek() {
                    return null;
                }

                @Override
                public Object poll() {
                    return null;
                }

                @Override
                public int size() {
                    return 0;
                }

                @Override
                public Iterator<Object> iterator() {
                    return ImmutableSet.of().iterator();
                }
            };

    /**
     * Queue that discards all elements.
     */
    @SuppressWarnings("unchecked") // impl never uses a parameter or returns any non-null value
    public static <E> Queue<E> discardingQueue() {
        return (Queue) DISCARDING_QUEUE;
    }

    /*
     * Note: All of this duplicate code sucks, but it saves a lot of memory. If only Java had mixins!
     * To maintain this code, make a change for the strong reference type. Then, cut and paste, and
     * replace "Strong" with "Soft" or "Weak" within the pasted text. The primary difference is that
     * strong entries store the key reference directly while soft and weak entries delegate to their
     * respective superclasses.
     */

    /**
     * Applies a supplemental hash function to a given hash code, which defends against poor quality
     * hash functions. This is critical when the concurrent hash map uses power-of-two length hash
     * tables, that otherwise encounter collisions for hash codes that do not differ in lower or upper
     * bits.
     *
     * @param h hash code
     */
    static int rehash(int h) {
        // Spread bits to regularize both segment and index locations,
        // using variant of single-word Wang/Jenkins hash.
        // TODO(kevinb): use Hashing/move this to Hashing?
        h += (h << 15) ^ 0xffffcd7d;
        h ^= (h >>> 10);
        h += (h << 3);
        h ^= (h >>> 6);
        h += (h << 2) + (h << 14);
        return h ^ (h >>> 16);
    }

    /**
     * This method is a convenience for testing. Code should call {@link Segment#newEntry} directly.
     */
    ReferenceEntry<K, V> newEntry(K key, int hash, ReferenceEntry<K, V> next) {
        Segment<K, V> segment = segmentFor(hash);
        segment.lock();
        try {
            return segment.newEntry(key, hash, next);
        } finally {
            segment.unlock();
        }
    }

    /**
     * This method is a convenience for testing. Code should call {@link Segment#copyEntry} directly.
     */
    // Guarded By Segment.this
    public ReferenceEntry<K, V> copyEntry(ReferenceEntry<K, V> original, ReferenceEntry<K, V> newNext) {
        int hash = original.getHash();
        return segmentFor(hash).copyEntry(original, newNext);
    }

    /**
     * This method is a convenience for testing. Code should call {@link Segment#setValue} instead.
     */
    // Guarded By Segment.this
    public ValueReference<K, V> newValueReference(ReferenceEntry<K, V> entry, V value, int weight) {
        int hash = entry.getHash();
        return valueStrength.referenceValue(segmentFor(hash), entry, checkNotNull(value), weight);
    }

    public static int mask = (1 << 23) - 1;

    /**
     * 定制hash算法。如果是整数直接返回
     *
     * @param key
     * @return
     */
    public int hash(Object key) {
       /* if (key instanceof Integer) {
            return (int) key;
        }
        if (key instanceof Long) {// 取1千万
            return (int) ((Long) key & mask);
        }*/
        int h = keyEquivalence.hash(key);
        return rehash(h);
    }

    public void reclaimValue(ValueReference<K, V> valueReference) {
        ReferenceEntry<K, V> entry = valueReference.getEntry();
        int hash = entry.getHash();
        segmentFor(hash).reclaimValue(entry.getKey(), hash, valueReference);
    }

    public void reclaimKey(ReferenceEntry<K, V> entry) {
        int hash = entry.getHash();
        segmentFor(hash).reclaimKey(entry, hash);
    }

    /**
     * This method is a convenience for testing. Code should call {@link Segment#getLiveValue}
     * instead.
     */
    boolean isLive(ReferenceEntry<K, V> entry, long now) {
        return segmentFor(entry.getHash()).getLiveValue(entry, now) != null;
    }

    /**
     * Returns the segment that should be used for a key with the given hash.
     *
     * @param hash the hash code for the key
     * @return the segment
     */
    private Segment<K, V> segmentFor(int hash) {
        // TODO(fry): Lazily create segments?
        int index = (hash >>> segmentShift) & segmentMask;
        return segments[index];
        //return segments[(hash >>> segmentShift) & segmentMask];
    }

    private Segment<K, V> createSegment(
            int initialCapacity, long maxSegmentWeight, StatsCounter statsCounter) {
        return new Segment<>(this, initialCapacity, maxSegmentWeight, statsCounter);
    }

    /**
     * Gets the value from an entry. Returns null if the entry is invalid, partially-collected,
     * loading, or expired. Unlike {@link Segment#getLiveValue} this method does not attempt to
     * cleanup stale entries. As such it should only be called outside of a segment context, such as
     * during iteration.
     */

    private V getLiveValue(ReferenceEntry<K, V> entry, long now) {
        if (entry.getKey() == null) {
            return null;
        }
        V value = entry.getValueReference().get();
        if (value == null) {
            return null;
        }

        if (isExpired(entry, now)) {
            return null;
        }
        return value;
    }

    // expiration

    /**
     * Returns true if the entry has expired.
     */
    public boolean isExpired(ReferenceEntry<K, V> entry, long now) {
        checkNotNull(entry);
        if (expiresAfterAccess() && (now - entry.getAccessTime() >= expireAfterAccessNanos)) {
            return true;
        }
        if (expiresAfterWrite() && (now - entry.getWriteTime() >= expireAfterWriteNanos)) {
            return true;
        }
        return false;
    }

    // queues

    // Guarded By Segment.this
    public static <K, V> void connectAccessOrder(ReferenceEntry<K, V> previous, ReferenceEntry<K, V> next) {
        previous.setNextInAccessQueue(next);
        next.setPreviousInAccessQueue(previous);
    }

    // Guarded By Segment.this
    public static <K, V> void nullifyAccessOrder(ReferenceEntry<K, V> nulled) {
        ReferenceEntry<K, V> nullEntry = nullEntry();
        nulled.setNextInAccessQueue(nullEntry);
        nulled.setPreviousInAccessQueue(nullEntry);
    }

    // Guarded By Segment.this
    public static <K, V> void connectWriteOrder(ReferenceEntry<K, V> previous, ReferenceEntry<K, V> next) {
        previous.setNextInWriteQueue(next);
        next.setPreviousInWriteQueue(previous);
    }

    // Guarded By Segment.this
    public static <K, V> void nullifyWriteOrder(ReferenceEntry<K, V> nulled) {
        ReferenceEntry<K, V> nullEntry = nullEntry();
        nulled.setNextInWriteQueue(nullEntry);
        nulled.setPreviousInWriteQueue(nullEntry);
    }

    /**
     * Notifies listeners that an entry has been automatically removed due to expiration, eviction, or
     * eligibility for garbage collection. This should be called every time expireEntries or
     * evictEntry is called (once the lock is released).
     */
    public void processPendingNotifications() {
        RemovalNotification<K, V> notification;
        while ((notification = removalNotificationQueue.poll()) != null) {
            try {
                removalListener.onRemoval(notification);
            } catch (Throwable e) {
                logger.info("Exception thrown by removal listener, msg = {}", e.getMessage(), e);
            }
        }
    }

    @SuppressWarnings("unchecked")
    final Segment<K, V>[] newSegmentArray(int ssize) {
        return new Segment[ssize];
    }

    // Inner Classes

    // Queues

    // Cache support

    public void cleanUp() {
        for (Segment<?, ?> segment : segments) {
            segment.cleanUp();
        }
    }

    // ConcurrentMap methods

    @Override
    public boolean isEmpty() {
        /*
         * Sum per-segment modCounts to avoid mis-reporting when elements are concurrently added and
         * removed in one segment while checking another, in which case the table was never actually
         * empty at any point. (The sum ensures accuracy up through at least 1<<31 per-segment
         * modifications before recheck.) Method containsValue() uses similar constructions for
         * stability checks.
         */
        long sum = 0L;
        Segment<K, V>[] segments = this.segments;
        for (int i = 0; i < segments.length; ++i) {
            if (segments[i].count != 0) {
                return false;
            }
            sum += segments[i].modCount;
        }

        if (sum != 0L) { // recheck unless no modifications
            for (int i = 0; i < segments.length; ++i) {
                if (segments[i].count != 0) {
                    return false;
                }
                sum -= segments[i].modCount;
            }
            if (sum != 0L) {
                return false;
            }
        }
        return true;
    }

    public long longSize() {
        Segment<K, V>[] segments = this.segments;
        long sum = 0;
        for (int i = 0; i < segments.length; ++i) {
            sum += Math.max(0, segments[i].count); // see https://github.com/google/guava/issues/2108
        }
        return sum;
    }

    @Override
    public int size() {
        return Ints.saturatedCast(longSize());
    }

    @Override
    public V get(Object key) {
        int hash = hash(key);
        return segmentFor(hash).get(key, hash);
    }

    public V get(K key, CacheLoader<? super K, V> loader) throws ExecutionException {
        int hash = hash(checkNotNull(key));
        kUnit unit = kRadix.getWeightByCode(hash, true);

        if (unit == null || unit.getWeight() <= kLoader.load()) {// K值校验
            hitRateCounter.recordMisses(1);
            try {
                return loader.load(key);//不走本地缓存
            } catch (Exception e) {
                throw new ExecutionException(e);
            }
        }
        hitRateCounter.recordHits(1);
        return segmentFor(hash).get(key, hash, loader);//走本地lru缓存
    }

    public V getIfPresent(Object key) {
        int hash = hash(checkNotNull(key));
        V value = segmentFor(hash).get(key, hash);
        if (value == null) {
            globalStatsCounter.recordMisses(1);
        } else {
            globalStatsCounter.recordHits(1);
        }
        return value;
    }

    // Only becomes available in Java 8 when it's on the interface.
    // @Override

    public V getOrDefault(Object key, V defaultValue) {
        V result = get(key);
        return (result != null) ? result : defaultValue;
    }

    public V getOrLoad(K key) throws ExecutionException {
        return get(key, defaultLoader);
    }

    public ImmutableMap<K, V> getAllPresent(Iterable<?> keys) {
        int hits = 0;
        int misses = 0;

        Map<K, V> result = Maps.newLinkedHashMap();
        for (Object key : keys) {
            V value = get(key);
            if (value == null) {
                misses++;
            } else {
                // TODO(fry): store entry key instead of query key
                @SuppressWarnings("unchecked")
                K castKey = (K) key;
                result.put(castKey, value);
                hits++;
            }
        }
        globalStatsCounter.recordHits(hits);
        globalStatsCounter.recordMisses(misses);
        return ImmutableMap.copyOf(result);
    }

    public ImmutableMap<K, V> getAll(Iterable<? extends K> keys) throws Exception {
        int hits = 0;
        int misses = 0;

        Map<K, V> result = Maps.newLinkedHashMap();
        Set<K> keysToLoad = Sets.newLinkedHashSet();
        for (K key : keys) {
            V value = get(key);
            if (!result.containsKey(key)) {
                result.put(key, value);
                if (value == null) {
                    misses++;
                    keysToLoad.add(key);
                } else {
                    hits++;
                }
            }
        }

        try {
            if (!keysToLoad.isEmpty()) {
                try {
                    Map<K, V> newEntries = loadAll(keysToLoad, defaultLoader);
                    for (K key : keysToLoad) {
                        V value = newEntries.get(key);
                        if (value == null) {
                            throw new CacheLoader.InvalidCacheLoadException("loadAll failed to return a value for " + key);
                        }
                        result.put(key, value);
                    }
                } catch (CacheLoader.UnsupportedLoadingOperationException e) {
                    // loadAll not implemented, fallback to load
                    for (K key : keysToLoad) {
                        misses--; // get will count this miss
                        result.put(key, get(key, defaultLoader));
                    }
                }
            }
            return ImmutableMap.copyOf(result);
        } finally {
            globalStatsCounter.recordHits(hits);
            globalStatsCounter.recordMisses(misses);
        }
    }

    /**
     * Returns the result of calling {@link CacheLoader#loadAll}, or null if {@code loader} doesn't
     * implement {@code loadAll}.
     */

    Map<K, V> loadAll(Set<? extends K> keys, CacheLoader<? super K, V> loader)
            throws ExecutionException {
        checkNotNull(loader);
        checkNotNull(keys);
        Stopwatch stopwatch = Stopwatch.createStarted();
        Map<K, V> result;
        boolean success = false;
        try {
            @SuppressWarnings("unchecked") // safe since all keys extend K
                    Map<K, V> map = (Map<K, V>) loader.loadAll(keys);
            result = map;
            success = true;
        } catch (CacheLoader.UnsupportedLoadingOperationException e) {
            success = true;
            throw e;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new ExecutionException(e);
        } catch (RuntimeException e) {
            throw new UncheckedExecutionException(e);
        } catch (Exception e) {
            throw new ExecutionException(e);
        } catch (Error e) {
            throw new ExecutionError(e);
        } finally {
            if (!success) {
                globalStatsCounter.recordLoadException(stopwatch.elapsed(NANOSECONDS));
            }
        }

        if (result == null) {
            globalStatsCounter.recordLoadException(stopwatch.elapsed(NANOSECONDS));
            throw new CacheLoader.InvalidCacheLoadException(loader + " returned null map from loadAll");
        }

        stopwatch.stop();
        // TODO(fry): batch by segment
        boolean nullsPresent = false;
        for (Entry<K, V> entry : result.entrySet()) {
            K key = entry.getKey();
            V value = entry.getValue();
            if (key == null || value == null) {
                // delay failure until non-null entries are stored
                nullsPresent = true;
            } else {
                put(key, value);
            }
        }

        if (nullsPresent) {
            globalStatsCounter.recordLoadException(stopwatch.elapsed(NANOSECONDS));
            throw new CacheLoader.InvalidCacheLoadException(loader + " returned null keys or values from loadAll");
        }

        // TODO(fry): record count of loaded entries
        globalStatsCounter.recordLoadSuccess(stopwatch.elapsed(NANOSECONDS));
        return result;
    }

    /**
     * Returns the internal entry for the specified key. The entry may be loading, expired, or
     * partially collected.
     */
    public ReferenceEntry<K, V> getEntry(Object key) {
        // does not impact recency ordering
        if (key == null) {
            return null;
        }
        int hash = hash(key);
        return segmentFor(hash).getEntry(key, hash);
    }

    public void refresh(K key) {
        int hash = hash(checkNotNull(key));
        segmentFor(hash).refresh(key, hash, defaultLoader, false);
    }

    @Override
    public boolean containsKey(Object key) {
        // does not impact recency ordering
        if (key == null) {
            return false;
        }
        int hash = hash(key);
        return segmentFor(hash).containsKey(key, hash);
    }

    @Override
    public boolean containsValue(Object value) {
        // does not impact recency ordering
        if (value == null) {
            return false;
        }

        // This implementation is patterned after ConcurrentHashMap, but without the locking. The only
        // way for it to return a false negative would be for the target value to jump around in the map
        // such that none of the subsequent iterations observed it, despite the fact that at every point
        // in time it was present somewhere int the map. This becomes increasingly unlikely as
        // CONTAINS_VALUE_RETRIES increases, though without locking it is theoretically possible.
        long now = ticker.read();
        final Segment<K, V>[] segments = this.segments;
        long last = -1L;
        for (int i = 0; i < CONTAINS_VALUE_RETRIES; i++) {
            long sum = 0L;
            for (Segment<K, V> segment : segments) {
                // ensure visibility of most recent completed write
                int unused = segment.count; // read-volatile

                AtomicReferenceArray<ReferenceEntry<K, V>> table = segment.table;
                for (int j = 0; j < table.length(); j++) {
                    for (ReferenceEntry<K, V> e = table.get(j); e != null; e = e.getNext()) {
                        V v = segment.getLiveValue(e, now);
                        if (v != null && valueEquivalence.equivalent(value, v)) {
                            return true;
                        }
                    }
                }
                sum += segment.modCount;
            }
            if (sum == last) {
                break;
            }
            last = sum;
        }
        return false;
    }

    @Override
    public V put(K key, V value) {
        checkNotNull(key);
        checkNotNull(value);
        int hash = hash(key);
        return segmentFor(hash).put(key, hash, value, false);
    }

    @Override
    public V putIfAbsent(K key, V value) {
        checkNotNull(key);
        checkNotNull(value);
        int hash = hash(key);
        return segmentFor(hash).put(key, hash, value, true);
    }

    @Override
    public V compute(K key, BiFunction<? super K, ? super V, ? extends V> function) {
        checkNotNull(key);
        checkNotNull(function);
        int hash = hash(key);
        return segmentFor(hash).compute(key, hash, function);
    }

    @Override
    public V computeIfAbsent(K key, Function<? super K, ? extends V> function) {
        checkNotNull(key);
        checkNotNull(function);
        return compute(key, (k, oldValue) -> (oldValue == null) ? function.apply(key) : oldValue);
    }

    @Override
    public V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> function) {
        checkNotNull(key);
        checkNotNull(function);
        return compute(key, (k, oldValue) -> (oldValue == null) ? null : function.apply(k, oldValue));
    }

    @Override
    public V merge(K key, V newValue, BiFunction<? super V, ? super V, ? extends V> function) {
        checkNotNull(key);
        checkNotNull(newValue);
        checkNotNull(function);
        return compute(
                key, (k, oldValue) -> (oldValue == null) ? newValue : function.apply(oldValue, newValue));
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Entry<? extends K, ? extends V> e : m.entrySet()) {
            put(e.getKey(), e.getValue());
        }
    }

    @Override
    public V remove(Object key) {
        if (key == null) {
            return null;
        }
        int hash = hash(key);
        return segmentFor(hash).remove(key, hash);
    }

    @Override
    public boolean remove(Object key, Object value) {
        if (key == null || value == null) {
            return false;
        }
        int hash = hash(key);
        return segmentFor(hash).remove(key, hash, value);
    }

    @Override
    public boolean replace(K key, V oldValue, V newValue) {
        checkNotNull(key);
        checkNotNull(newValue);
        if (oldValue == null) {
            return false;
        }
        int hash = hash(key);
        return segmentFor(hash).replace(key, hash, oldValue, newValue);
    }

    @Override
    public V replace(K key, V value) {
        checkNotNull(key);
        checkNotNull(value);
        int hash = hash(key);
        return segmentFor(hash).replace(key, hash, value);
    }

    @Override
    public void clear() {
        for (Segment<K, V> segment : segments) {
            segment.clear();
        }
    }

    public void invalidateAll(Iterable<?> keys) {
        // TODO(fry): batch by segment
        for (Object key : keys) {
            remove(key);
        }
    }

    Set<K> keySet;

    @Override
    public Set<K> keySet() {
        // does not impact recency ordering
        Set<K> ks = keySet;
        return (ks != null) ? ks : (keySet = new KeySet(this));
    }

    Collection<V> values;

    @Override
    public Collection<V> values() {
        // does not impact recency ordering
        Collection<V> vs = values;
        return (vs != null) ? vs : (values = new Values(this));
    }

    Set<Entry<K, V>> entrySet;

    @Override
    public Set<Entry<K, V>> entrySet() {
        // does not impact recency ordering
        Set<Entry<K, V>> es = entrySet;
        return (es != null) ? es : (entrySet = new EntrySet(this));
    }

    // Iterator Support

    abstract class HashIterator<T> implements Iterator<T> {

        int nextSegmentIndex;
        int nextTableIndex;
        Segment<K, V> currentSegment;
        AtomicReferenceArray<ReferenceEntry<K, V>> currentTable;
        ReferenceEntry<K, V> nextEntry;
        WriteThroughEntry nextExternal;
        WriteThroughEntry lastReturned;

        HashIterator() {
            nextSegmentIndex = segments.length - 1;
            nextTableIndex = -1;
            advance();
        }

        @Override
        public abstract T next();

        final void advance() {
            nextExternal = null;

            if (nextInChain()) {
                return;
            }

            if (nextInTable()) {
                return;
            }

            while (nextSegmentIndex >= 0) {
                currentSegment = segments[nextSegmentIndex--];
                if (currentSegment.count != 0) {
                    currentTable = currentSegment.table;
                    nextTableIndex = currentTable.length() - 1;
                    if (nextInTable()) {
                        return;
                    }
                }
            }
        }

        /**
         * Finds the next entry in the current chain. Returns true if an entry was found.
         */
        boolean nextInChain() {
            if (nextEntry != null) {
                for (nextEntry = nextEntry.getNext(); nextEntry != null; nextEntry = nextEntry.getNext()) {
                    if (advanceTo(nextEntry)) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Finds the next entry in the current table. Returns true if an entry was found.
         */
        boolean nextInTable() {
            while (nextTableIndex >= 0) {
                if ((nextEntry = currentTable.get(nextTableIndex--)) != null) {
                    if (advanceTo(nextEntry) || nextInChain()) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Advances to the given entry. Returns true if the entry was valid, false if it should be
         * skipped.
         */
        boolean advanceTo(ReferenceEntry<K, V> entry) {
            try {
                long now = ticker.read();
                K key = entry.getKey();
                V value = getLiveValue(entry, now);
                if (value != null) {
                    nextExternal = new WriteThroughEntry(key, value);
                    return true;
                } else {
                    // Skip stale entry.
                    return false;
                }
            } finally {
                currentSegment.postReadCleanup();
            }
        }

        @Override
        public boolean hasNext() {
            return nextExternal != null;
        }

        WriteThroughEntry nextEntry() {
            if (nextExternal == null) {
                throw new NoSuchElementException();
            }
            lastReturned = nextExternal;
            advance();
            return lastReturned;
        }

        @Override
        public void remove() {
            checkState(lastReturned != null);
            LocalCache.this.remove(lastReturned.getKey());
            lastReturned = null;
        }
    }

    final class KeyIterator extends HashIterator<K> {

        @Override
        public K next() {
            return nextEntry().getKey();
        }
    }

    final class ValueIterator extends HashIterator<V> {

        @Override
        public V next() {
            return nextEntry().getValue();
        }
    }

    /**
     * Custom Entry class used by EntryIterator.next(), that relays setValue changes to the underlying
     * map.
     */
    final class WriteThroughEntry implements Entry<K, V> {
        final K key; // non-null
        V value; // non-null

        WriteThroughEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public boolean equals(Object object) {
            // Cannot use key and value equivalence
            if (object instanceof Entry) {
                Entry<?, ?> that = (Entry<?, ?>) object;
                return key.equals(that.getKey()) && value.equals(that.getValue());
            }
            return false;
        }

        @Override
        public int hashCode() {
            // Cannot use key and value equivalence
            return key.hashCode() ^ value.hashCode();
        }

        @Override
        public V setValue(V newValue) {
            V oldValue = put(key, newValue);
            value = newValue; // only if put succeeds
            return oldValue;
        }

        @Override
        public String toString() {
            return getKey() + "=" + getValue();
        }
    }

    final class EntryIterator extends HashIterator<Entry<K, V>> {

        @Override
        public Entry<K, V> next() {
            return nextEntry();
        }
    }

    public static <E> ArrayList<E> toArrayList(Collection<E> c) {
        // Avoid calling ArrayList(Collection), which may call back into toArray.
        ArrayList<E> result = new ArrayList<E>(c.size());
        Iterators.addAll(result, c.iterator());
        return result;
    }

    boolean removeIf(BiPredicate<? super K, ? super V> filter) {
        checkNotNull(filter);
        boolean changed = false;
        for (K key : keySet()) {
            while (true) {
                V value = get(key);
                if (value == null || !filter.test(key, value)) {
                    break;
                } else if (LocalCache.this.remove(key, value)) {
                    changed = true;
                    break;
                }
            }
        }
        return changed;
    }

    final class KeySet extends AbstractCacheSet<K> {

        KeySet(ConcurrentMap<?, ?> map) {
            super(map);
        }

        @Override
        public Iterator<K> iterator() {
            return new KeyIterator();
        }

        @Override
        public boolean contains(Object o) {
            return map.containsKey(o);
        }

        @Override
        public boolean remove(Object o) {
            return map.remove(o) != null;
        }
    }

    final class Values extends AbstractCollection<V> {
        private final ConcurrentMap<?, ?> map;

        Values(ConcurrentMap<?, ?> map) {
            this.map = map;
        }

        @Override
        public int size() {
            return map.size();
        }

        @Override
        public boolean isEmpty() {
            return map.isEmpty();
        }

        @Override
        public void clear() {
            map.clear();
        }

        @Override
        public Iterator<V> iterator() {
            return new ValueIterator();
        }

        @Override
        public boolean removeIf(Predicate<? super V> filter) {
            checkNotNull(filter);
            return LocalCache.this.removeIf((k, v) -> filter.test(v));
        }

        @Override
        public boolean contains(Object o) {
            return map.containsValue(o);
        }

        // super.toArray() may misbehave if size() is inaccurate, at least on old versions of Android.
        // https://code.google.com/p/android/issues/detail?id=36519 / http://r.android.com/47508

        @Override
        public Object[] toArray() {
            return toArrayList(this).toArray();
        }

        @Override
        public <E> E[] toArray(E[] a) {
            return toArrayList(this).toArray(a);
        }
    }

    final class EntrySet extends AbstractCacheSet<Entry<K, V>> {

        EntrySet(ConcurrentMap<?, ?> map) {
            super(map);
        }

        @Override
        public Iterator<Entry<K, V>> iterator() {
            return new EntryIterator();
        }

        @Override
        public boolean removeIf(Predicate<? super Entry<K, V>> filter) {
            checkNotNull(filter);
            return LocalCache.this.removeIf((k, v) -> filter.test(Maps.immutableEntry(k, v)));
        }

        @Override
        public boolean contains(Object o) {
            if (!(o instanceof Entry)) {
                return false;
            }
            Entry<?, ?> e = (Entry<?, ?>) o;
            Object key = e.getKey();
            if (key == null) {
                return false;
            }
            V v = LocalCache.this.get(key);

            return v != null && valueEquivalence.equivalent(e.getValue(), v);
        }

        @Override
        public boolean remove(Object o) {
            if (!(o instanceof Entry)) {
                return false;
            }
            Entry<?, ?> e = (Entry<?, ?>) o;
            Object key = e.getKey();
            return key != null && LocalCache.this.remove(key, e.getValue());
        }
    }
}
