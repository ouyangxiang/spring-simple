package com.example.liuyaohua.cache.lruk.lru;

import com.example.liuyaohua.cache.lruk.k.KLoader;
import com.example.liuyaohua.cache.lruk.k.KRadix;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.example.liuyaohua.cache.lruk.k.KLoader;
import com.example.liuyaohua.cache.lruk.k.KRadix;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 作者 yaohua.liu
 * 日期 2018-03-24 12:36
 * 说明 LocalLoadingCache is a wrapper around LocalCache for a lruk with loading.
 */
public class LocalLoadingCache<K, V> extends LocalManualCache<K, V> implements LoadingCache<K, V> {

    public LocalLoadingCache(CacheBuilder<? super K, ? super V> builder, CacheLoader<? super K, V> loader, KLoader kLoader) {
        super(builder, checkNotNull(loader), checkNotNull(kLoader));
    }

    // Cache methods

    @Override
    public V get(K key) throws ExecutionException {
        return localCache.getOrLoad(key);
    }

    @Override
    public V getUnchecked(K key) {
        try {
            return get(key);
        } catch (ExecutionException e) {
            throw new UncheckedExecutionException(e.getCause());
        }
    }

    @Override
    public final V apply(K key) {
        return getUnchecked(key);
    }

    @Override
    public ImmutableMap<K, V> getAll(Iterable<? extends K> keys) throws ExecutionException {
        Map<K, V> map = new HashMap<K, V>();
        for (K key : keys) {
            map.put(key, localCache.getOrLoad(key));
        }
        return ImmutableMap.copyOf(map);
    }

    @Override
    public void refresh(K key) {
        throw new UnsupportedOperationException();
    }

    /**
     * 根据key删除对应的值
     *
     * @param key K
     * @return V
     */
    public V remove(K key) {
        return localCache.remove(key);
    }

    /**
     * 是否包含key
     *
     * @param key
     * @return
     */
    public boolean containsKey(K key){
        return localCache.containsKey(key);
    }

    /**
     * 获取 K 结构
     * @return KRadix
     */
    public KRadix getKRadix(){
        return localCache.getkRadix();
    }
    /**
     * 定制hash算法。如果是整数直接返回
     *
     * @param key
     * @return
     */
    public int hash(Object key){
        return localCache.hash(key);
    }
}

