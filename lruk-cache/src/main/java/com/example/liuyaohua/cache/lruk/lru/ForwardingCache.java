/*
 * Copyright (C) 2011 The Guava Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.example.liuyaohua.cache.lruk.lru;

import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;
import com.google.common.collect.ImmutableMap;
import com.example.liuyaohua.cache.lruk.base.ForwardingObject;
import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;

/**
 * A lruk which forwards all its method calls to another lruk. Subclasses should override one or
 * more methods to modify the behavior of the backing lruk as desired per the <a
 * href="http://en.wikipedia.org/wiki/Decorator_pattern">decorator pattern</a>.
 *
 * @author Charles Fry
 * @since 10.0
 */
public abstract class ForwardingCache<K, V> extends ForwardingObject implements Cache<K, V> {

    /**
     * Constructor for use by subclasses.
     */
    protected ForwardingCache() {
    }

    @Override
    protected abstract Cache<K, V> delegate();

    /**
     * @since 11.0
     */
    @Override
    public V getIfPresent(Object key) {
        return delegate().getIfPresent(key);
    }

    /**
     * @since 11.0
     */
    @Override
    public V get(K key, Callable<? extends V> valueLoader) throws Exception {
        return delegate().get(key, valueLoader);
    }

    /**
     * @since 11.0
     */
    @Override
    public ImmutableMap<K, V> getAllPresent(Iterable<?> keys) {
        return delegate().getAllPresent(keys);
    }

    /**
     * @since 11.0
     */
    @Override
    public void put(K key, V value) {
        delegate().put(key, value);
    }

    /**
     * @since 12.0
     */
    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        delegate().putAll(m);
    }

    @Override
    public void invalidate(Object key) {
        delegate().invalidate(key);
    }

    /**
     * @since 11.0
     */
    @Override
    public void invalidateAll(Iterable<?> keys) {
        delegate().invalidateAll(keys);
    }

    @Override
    public void invalidateAll() {
        delegate().invalidateAll();
    }

    @Override
    public long size() {
        return delegate().size();
    }

    @Override
    public CacheStats stats() {
        return delegate().stats();
    }

    @Override
    public CacheStats kHitRateStats() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ConcurrentMap<K, V> asMap() {
        return delegate().asMap();
    }

    @Override
    public void cleanUp() {
        delegate().cleanUp();
    }
}
