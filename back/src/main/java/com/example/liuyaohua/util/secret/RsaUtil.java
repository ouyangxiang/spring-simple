package com.example.liuyaohua.util.secret;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * @author qibin.xia rsa加密解密算法
 */
public class RsaUtil {

	/**
	 * 随机生成密钥对
	 */
	public static void genKeyPair() {
		KeyPairGenerator keyPairGen = null;
		try {
			keyPairGen = KeyPairGenerator.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
		keyPairGen.initialize(1024, new SecureRandom());
		KeyPair keyPair = keyPairGen.generateKeyPair();
		System.out.println("私钥：" + Base64.encode(keyPair.getPrivate().getEncoded()));
		System.out.println("公钥：" + Base64.encode(keyPair.getPublic().getEncoded()));
	}
	
	/**
	 * rsa加密
	 * 
	 * @param publicKey
	 * @param data
	 * @return
	 */
	public static String encrypt(String publicKey, String data) {
		RSAPublicKey rsaPublicKey = loadPublicKey(publicKey);
		return new String(Base64.encode(encrypt(rsaPublicKey, data.getBytes())));
//		return new String(encrypt(rsaPublicKey, data.getBytes()));
	}
	
	/**
	/**
	 * rsa加密
	 * 
	 * @param publicKey
	 * @param data
	 * @return
	 */
	public static String encrypt(InputStream publicKey, String data) {
		RSAPublicKey rsaPublicKey = loadPublicKey(publicKey);
		return new String(Base64.encode(encrypt(rsaPublicKey, data.getBytes())));
	}
	
	/**
	 * rsa解密
	 * 
	 * @param privateKey
	 * @param data
	 * @return
	 */
	public static String decrypt(String privateKey, String data) {
		RSAPrivateKey rsaPrivateKey = loadPrivateKey(privateKey);
		return new String(decrypt(rsaPrivateKey, Base64.decode(data)));
	}
	
	/**
	 * rsa解密
	 * 
	 * @param privateKey
	 * @param data
	 * @return
	 */
    public static String decrypt(InputStream privateKey, String data) {
    	RSAPrivateKey rsaPrivateKey = loadPrivateKey(privateKey);
    	return new String(decrypt(rsaPrivateKey, Base64.decode(data)));
	}
	
	/**
	 * 从文件中输入流中加载公钥
	 * @param in 公钥输入流
	 */
	private static RSAPublicKey loadPublicKey(InputStream in){
		try {
			BufferedReader br= new BufferedReader(new InputStreamReader(in));
			String readLine= null;
			StringBuilder sb= new StringBuilder();
			while((readLine= br.readLine())!=null){
				if(readLine.charAt(0)=='-'){
					continue;
				}else{
					sb.append(readLine);
					sb.append('\r');
				}
			}
			return loadPublicKey(sb.toString());
		} catch (IOException e) {
			throw new RuntimeException("公钥数据流读取错误", e);
		} catch (NullPointerException e) {
			throw new RuntimeException("公钥输入流为空", e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	/**
	 * 从字符串中加载公钥
	 * @param publicKeyStr 公钥数据字符串
	 */
	private static RSAPublicKey loadPublicKey(String publicKeyStr){
		try {
			byte[] buffer=Base64.decode(publicKeyStr);
			KeyFactory keyFactory= KeyFactory.getInstance("RSA");
			X509EncodedKeySpec keySpec= new X509EncodedKeySpec(buffer);
			return (RSAPublicKey) keyFactory.generatePublic(keySpec);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("无此算法", e);
		} catch (InvalidKeySpecException e) {
			throw new RuntimeException("公钥非法", e);
		} catch (NullPointerException e) {
			throw new RuntimeException("公钥数据为空", e);
		}
	}

	/**
	 * 从文件中加载私钥
	 * @param
	 */
	private static RSAPrivateKey loadPrivateKey(InputStream in){
		try {
			BufferedReader br= new BufferedReader(new InputStreamReader(in));
			String readLine= null;
			StringBuilder sb= new StringBuilder();
			while((readLine= br.readLine())!=null){
				if(readLine.charAt(0)=='-'){
					continue;
				}else{
					sb.append(readLine);
					sb.append('\r');
				}
			}
			return loadPrivateKey(sb.toString());
		} catch (IOException e) {
			throw new RuntimeException("私钥数据读取错误", e);
		} catch (NullPointerException e) {
			throw new RuntimeException("私钥输入流为空", e);
		}
	}

	private static RSAPrivateKey loadPrivateKey(String privateKeyStr){
		try {
			byte[] buffer= Base64.decode(privateKeyStr);
			PKCS8EncodedKeySpec keySpec= new PKCS8EncodedKeySpec(buffer);
			KeyFactory keyFactory= KeyFactory.getInstance("RSA");
			return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("无此算法", e);
		} catch (InvalidKeySpecException e) {
			throw new RuntimeException("私钥非法", e);
		} catch (NullPointerException e) {
			throw new RuntimeException("私钥数据为空", e);
		}
	}

	/**
	 * 加密过程
	 * @param publicKey 公钥
	 * @param plainTextData 明文数据
	 * @return
	 */
	private static byte[] encrypt(RSAPublicKey publicKey, byte[] plainTextData){
		if(publicKey== null){
			throw new RuntimeException("加密公钥为空, 请设置");
		}
		Cipher cipher= null;
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			return cipher.doFinal(plainTextData);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("无此加密算法", e);
		} catch (NoSuchPaddingException e) {
			throw new RuntimeException(e);
		}catch (InvalidKeyException e) {
			throw new RuntimeException("加密公钥非法,请检查", e);
		} catch (IllegalBlockSizeException e) {
			throw new RuntimeException("明文长度非法", e);
		} catch (BadPaddingException e) {
			throw new RuntimeException("明文数据已损坏", e);
		}
	}

	/**
	 * 解密过程
	 * @param privateKey 私钥
	 * @param cipherData 密文数据
	 * @return 明文数据
	 */
	private static byte[] decrypt(RSAPrivateKey privateKey, byte[] cipherData){
		if (privateKey== null){
			throw new RuntimeException("解密私钥为空, 请设置");
		}
		Cipher cipher= null;
		try {
			cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return cipher.doFinal(cipherData);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("无此解密算法");
		} catch (NoSuchPaddingException e) {
			throw new RuntimeException(e);
		}catch (InvalidKeyException e) {
			throw new RuntimeException("解密私钥非法,请检查", e);
		} catch (IllegalBlockSizeException e) {
			throw new RuntimeException("密文长度非法", e);
		} catch (BadPaddingException e) {
			throw new RuntimeException("密文数据已损坏", e);
		}		
	}
	/** 
     * 私钥解密 
     *  
     * @param data 
     * @param privateKey 
     * @return 
     * @throws Exception 
     */  
    public static String decryptByPrivateKey(String data, RSAPrivateKey privateKey)  
            throws Exception {  
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.DECRYPT_MODE, privateKey);  
        //模长  
        int key_len = privateKey.getModulus().bitLength() / 8;  
        byte[] bytes = data.getBytes();  
        byte[] bcd = ASCII_To_BCD(bytes, bytes.length);  
        System.err.println(bcd.length);  
        //如果密文长度大于模长则要分组解密  
        String ming = "";  
        byte[][] arrays = splitArray(bcd, key_len);  
        for(byte[] arr : arrays){  
            ming += new String(cipher.doFinal(arr));  
        }  
        return ming;  
    } 
    /** 
     * ASCII码转BCD码 
     *  
     */  
    public static byte[] ASCII_To_BCD(byte[] ascii, int asc_len) {  
        byte[] bcd = new byte[asc_len / 2];  
        int j = 0;  
        for (int i = 0; i < (asc_len + 1) / 2; i++) {  
            bcd[i] = asc_to_bcd(ascii[j++]);  
            bcd[i] = (byte) (((j >= asc_len) ? 0x00 : asc_to_bcd(ascii[j++])) + (bcd[i] << 4));  
        }  
        return bcd;  
    } 
    public static byte asc_to_bcd(byte asc) {  
        byte bcd;  
  
        if ((asc >= '0') && (asc <= '9'))  
            bcd = (byte) (asc - '0');  
        else if ((asc >= 'A') && (asc <= 'F'))  
            bcd = (byte) (asc - 'A' + 10);  
        else if ((asc >= 'a') && (asc <= 'f'))  
            bcd = (byte) (asc - 'a' + 10);  
        else  
            bcd = (byte) (asc - 48);  
        return bcd;  
    }  
    /** 
     *拆分数组  
     */  
    public static byte[][] splitArray(byte[] data,int len){  
        int x = data.length / len;  
        int y = data.length % len;  
        int z = 0;  
        if(y!=0){  
            z = 1;  
        }  
        byte[][] arrays = new byte[x+z][];  
        byte[] arr;  
        for(int i=0; i<x+z; i++){  
            arr = new byte[len];  
            if(i==x+z-1 && y!=0){  
                System.arraycopy(data, i*len, arr, 0, y);  
            }else{  
                System.arraycopy(data, i*len, arr, 0, len);  
            }  
            arrays[i] = arr;  
        }  
        return arrays;  
    }  
    public static void main(String[] args) throws Exception {
		String RSA_PRIVATE_KEY="MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALUnJVlQ05ze9UHj8lVD9Nd0EO+7RDkiojHsahujXDBxsDWTheAWWOFFxJs2H+acFFn0h617YwVqyv9GUQJy1rUsvDyRcSajV9FjF7pxzqgypR6GiaKCbxF37rvu9tVcQu7fc199kCjpcfMzGtZxUsHkvG7lT16q7oZA3D0Xe0sbAgMBAAECgYA5CbFV1QA8JWt9qqMTu8jsycvHX+RURRZit7iVKTOOwU0lIWUGsKKdk5Ua9cjHmJ9Dz52kvvPnsb9m7yH0rmnxrBinYbIZW2MU7jXQf49T3RlZKduZnY/jFfpk+5eEk/tck7rcS6mA7/YbdnYWBxlDTIRTv0TZxwCN7OaTDrkv4QJBANtirxCT/kOEL3WixoCl45G7sD5h/puz0M+AuLeSgWrKQzTljJp9GTf98PLQ5JBZSTlAUwAdVQ1l6hP434v8y+sCQQDTYvaKokpDA6cSjeSNxq4eXBjgK3fFUMmpBO7uvEwojiN7tyxKlMJ79R0nJbNWW4MLShWSvLAIXvOFZkZw56GRAkAfF3VIHDFE0E8JlPAc+2WtGbR4otq6pllJjyyel5zPzaqIrJ+opWIVlV+4ifJaoszF2F/q/D05kOADxJmp9UHRAkEAmKNeOS8FBqztiFVTUxjDBAMmp0BcBOvYIicAzPzE1YgFkUDsR5JxeQa+nVT40YPU9WLyfvmm9vRxTcNTXBW2MQJAaA24sFLOrHBMMtJgD/UBkOwVHRpwLIUdFCkvN1tiIJfSso0+tHIcxI1khSaWtP/RCGaOE3A8r6ej6/izbjTS9A==";
		String RSE_PUBLIC_KEY="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC1JyVZUNOc3vVB4/JVQ/TXdBDvu0Q5IqIx7Gobo1wwcbA1k4XgFljhRcSbNh/mnBRZ9Iete2MFasr/RlECcta1LLw8kXEmo1fRYxe6cc6oMqUehomigm8Rd+677vbVXELu33NffZAo6XHzMxrWcVLB5Lxu5U9equ6GQNw9F3tLGwIDAQAB";

		String s1="qd1231234123412";
		System.out.println(s1);
		String s2=RsaUtil.encrypt(RSE_PUBLIC_KEY, s1);
		System.out.println(RsaUtil.decrypt(RSA_PRIVATE_KEY,s2));
    }
}
