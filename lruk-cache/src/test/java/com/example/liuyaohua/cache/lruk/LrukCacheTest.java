package com.example.liuyaohua.cache.lruk;

import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.example.liuyaohua.cache.lruk.lru.status.CacheStats;
import junit.framework.TestCase;
import org.junit.Assert;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-03-16 19:16
 * <p>说明 ...
 */
public class LrukCacheTest extends TestCase {

    MutableInteger om = new MutableInteger(10000, 10000);
    MutableInteger nm = new MutableInteger(2, 3);
    Integer key = 1;


    public void testGet() throws Exception {
        LrukSimple cache = new LrukSimple(2);
        Assert.assertTrue(!cache.containsKey(key));
        cache.put(key, nm);
        Assert.assertTrue(cache.containsKey(key));
        MutableInteger m = cache.get(key);
        Assert.assertNotNull(m);
        Assert.assertEquals(om.getKey(), m.getKey());
        Assert.assertEquals(om.getValue(), m.getValue());

        getForNTimes(cache, key, 5);

        m = cache.get(key);
        Assert.assertNotNull(m);
        Assert.assertEquals(nm.getKey(), m.getKey());
        Assert.assertEquals(nm.getValue(), m.getValue());
    }

    public void testGetUnchecked() throws InterruptedException {
        LrukSimple cache = new LrukSimple(2);
        Assert.assertTrue(!cache.containsKey(key));
        cache.put(key, nm);
        Assert.assertTrue(cache.containsKey(key));
        MutableInteger m = cache.getUnchecked(key);
        Assert.assertNotNull(m);
        Assert.assertEquals(om.getKey(), m.getKey());
        Assert.assertEquals(om.getValue(), m.getValue());

        getUncheckedForNTimes(cache, key, 5);

        m = cache.getUnchecked(key);
        Assert.assertNotNull(m);
        Assert.assertEquals(nm.getKey(), m.getKey());
        Assert.assertEquals(nm.getValue(), m.getValue());
    }

    public void testGetFromLocalCache() throws Exception {
        LrukSimple cache = new LrukSimple(2);
        Assert.assertTrue(!cache.containsKey(key));
        cache.put(key, nm);
        Assert.assertTrue(cache.containsKey(key));
        MutableInteger m = cache.get(key, false);
        Assert.assertNotNull(m);
        Assert.assertEquals(om.getKey(), m.getKey());
        Assert.assertEquals(om.getValue(), m.getValue());

        getForNTimes(cache, key, 5);

        m = cache.get(key, false);
        Assert.assertNotNull(m);
        Assert.assertEquals(om.getKey(), m.getKey());
        Assert.assertEquals(om.getValue(), m.getValue());
    }

    public void testGetAll() throws Exception {
        LrukSimple cache = new LrukSimple(3);
        ImmutableMap map = cache.getAll(Lists.newArrayList(1, 2, 3));
        for (Object o : map.keySet()) {
            Integer i = (Integer) o;
            if (i == null) {
                continue;
            }
            MutableInteger m = (MutableInteger) map.get(i);
            Assert.assertNotNull(m);
            Assert.assertEquals(om.getKey(), m.getKey());
            Assert.assertEquals(om.getValue(), m.getValue());
        }

        for (int i = 1; i <= 4; i++) {
            cache.put(i, new MutableInteger(i * 10, i * 20));
            getUncheckedForNTimes(cache, i, 5);
        }

        map = cache.getAll(Lists.newArrayList(1, 2, 3));
        Assert.assertNotNull(map);
        Assert.assertEquals(3, map.size());

        for (Object o : map.keySet()) {
            Integer i = (Integer) o;
            if (i == null) {
                continue;
            }
            MutableInteger m = (MutableInteger) map.get(i);
            Assert.assertNotNull(m);
            Assert.assertEquals(i * 10, m.getKey());
            Assert.assertEquals(i * 20, m.getValue());
        }

        map = cache.getAll(Lists.newArrayList(1, 6, 5));
        for (Object o : map.keySet()) {
            Integer i = (Integer) o;
            if (i == null) {
                continue;
            }
            MutableInteger m = (MutableInteger) map.get(i);
            Assert.assertNotNull(m);
            if (i == 1) {
                Assert.assertEquals(i * 10, m.getKey());
                Assert.assertEquals(i * 20, m.getValue());
                continue;
            }
            Assert.assertEquals(om.getKey(), m.getKey());
            Assert.assertEquals(om.getValue(), m.getValue());
        }
    }

    public void testPut() throws Exception {
        testGet();
    }

    public void testContainsKey() throws Exception {
        testGet();
    }

    public void testRemove() throws Exception {
        LrukSimple cache = new LrukSimple(2);
        // 缓存不存在时，返回null
        MutableInteger r = cache.remove(key);
        Assert.assertNull(r);
        // 向缓存中添加数据
        cache.put(key, nm);
        Assert.assertTrue(cache.containsKey(key));
        // 请求5次后，超过阀值
        getForNTimes(cache, key, 5);
        r = cache.get(key);
        Thread.sleep(5);
        Assert.assertNotNull(r);
        Assert.assertEquals(nm.getKey(), r.getKey());
        Assert.assertEquals(nm.getValue(), r.getValue());
        // 移除key
        MutableInteger m = cache.remove(key);
        Assert.assertNotNull(m);
        Assert.assertEquals(nm.getKey(), r.getKey());
        Assert.assertEquals(nm.getValue(), r.getValue());
        Thread.sleep(25);
        // 检查本地缓存是否还存在
        Assert.assertTrue(!cache.containsKey(key));
    }

    public void testReplace() throws Exception {
        LrukSimple cache = new LrukSimple(2);
        // 缓存不存在时，返回null
        Assert.assertTrue(!cache.containsKey(key));
        cache.put(key, nm);
        Assert.assertTrue(cache.containsKey(key));
        // 请求5次后，超过阀值
        getForNTimes(cache, key, 5);
        MutableInteger r = cache.get(key);
        Thread.sleep(5);
        Assert.assertNotNull(r);
        Assert.assertEquals(nm.getKey(), r.getKey());
        Assert.assertEquals(nm.getValue(), r.getValue());
        // 开始替换
        MutableInteger m = new MutableInteger(123, 123);
        r = cache.replace(key, m);
        Thread.sleep(5);
        Assert.assertNotNull(r);
        Assert.assertEquals(nm.getKey(), r.getKey());
        Assert.assertEquals(nm.getValue(), r.getValue());
        //检查替换后的值是否与已知的一样
        r = cache.get(key);
        Assert.assertNotNull(r);
        Assert.assertEquals(m.getKey(), r.getKey());
        Assert.assertEquals(m.getValue(), r.getValue());
    }

    public void testLoad() {
        LrukSimple cache = new LrukSimple(2);
        MutableInteger m = cache.load(key);
        Assert.assertNotNull(m);
        Assert.assertEquals(om.getKey(), m.getKey());
        Assert.assertEquals(om.getValue(), m.getValue());
    }

    public void testReload() throws Exception {
        LrukSimple cache = new LrukSimple(2);
        cache.put(key, nm);
        Thread.sleep(25);

        getForNTimes(cache, key, 5);
        MutableInteger r = cache.get(key);
        Thread.sleep(5);
        Assert.assertNotNull(r);
        Assert.assertEquals(nm.getKey(), r.getKey());
        Assert.assertEquals(nm.getValue(), r.getValue());

        cache.reload(key);

        r = cache.get(key);
        Assert.assertNotNull(r);
        Assert.assertEquals(om.getKey(), r.getKey());
        Assert.assertEquals(om.getValue(), r.getValue());
    }

    public void testSize() throws InterruptedException {
        LrukSimple cache = new LrukSimple(2);
        Assert.assertEquals(0, cache.size());
        for (int i = 0; i < 100; i++) {
            cache.put(i, new MutableInteger(i * 10, i * 20));
        }
        Thread.sleep(25);
        Assert.assertEquals(100, cache.size());
        for (int i = 0; i < 50; i++) {
            cache.remove(i);
        }

        Thread.sleep(50);
        Assert.assertEquals(100 - 50, cache.size());
    }

    /**
     * 测试缓存最大的缓存数量不能超过设置的值
     */
    public void testMaximumSize() throws InterruptedException {
        LrukSimple cache = new LrukSimple(3);

        for (int i = 1; i <= 2048; i++) {
            //if (i % 5 == 0) {
                getUncheckedForNTimes(cache, i, 5);
            //    continue;
           // }
           // cache.getUnchecked(i);
            //Thread.sleep(1);
        }
        Thread.sleep(500);
        Assert.assertEquals(cache.size(), 1024);
    }

    /**
     * 测试命中率（使用sleep把异步做成同步）
     */
    public void testHitRate() throws Exception {
        LrukSimple cache = new LrukSimple(2);
        CacheStats cacheStats;

        int i = 1;
        cache.get(i);
        Thread.sleep(5);
        cacheStats = cache.kHitRateStats();
        Assert.assertNotNull(cacheStats);
        Assert.assertEquals(0, (int) (cacheStats.hitRate() * 100));

        cache.get(i);
        Thread.sleep(5);
        cacheStats = cache.kHitRateStats();
        Assert.assertNotNull(cacheStats);
        Assert.assertEquals(0, (int) (cacheStats.hitRate() * 100));

        cache.get(i);
        Thread.sleep(5);
        cache.get(i);
        Thread.sleep(5);
        cacheStats = cache.kHitRateStats();
        Assert.assertNotNull(cacheStats);
        Assert.assertEquals(50, (int) (cacheStats.hitRate() * 100));

        cache.get(i);
        Thread.sleep(5);
        cacheStats = cache.kHitRateStats();
        Assert.assertNotNull(cacheStats);
        Assert.assertEquals(60, (int) (cacheStats.hitRate() * 100));

        cache.get(i);
        Thread.sleep(5);
        cache.get(i);
        Thread.sleep(5);
        cache.get(i);
        Thread.sleep(5);
        cacheStats = cache.kHitRateStats();
        Assert.assertNotNull(cacheStats);
        Assert.assertEquals(75, (int) (cacheStats.hitRate() * 100));
        cache.get(i);
        Thread.sleep(5);
        cache.get(i);
        Thread.sleep(5);
        cacheStats = cache.kHitRateStats();
        Assert.assertNotNull(cacheStats);
        Assert.assertEquals(80, (int) (cacheStats.hitRate() * 100));
    }

    private void getForNTimes(LrukCache<Integer, MutableInteger> cache, Integer key, int n) throws Exception {
        if (cache == null) {
            return;
        }
        if (n <= 0) {
            return;
        }
        for (int i = 0; i < n; i++) {
            cache.get(key);
        }
    }

    private void getUncheckedForNTimes(LrukCache<Integer, MutableInteger> cache, Integer key, int n) {
        if (cache == null) {
            return;
        }
        if (n <= 0) {
            return;
        }
        for (int i = 0; i < n; i++) {
            cache.getUnchecked(key);
        }
    }
}

