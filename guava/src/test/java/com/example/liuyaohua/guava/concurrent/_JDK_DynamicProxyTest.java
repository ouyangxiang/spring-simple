package com.example.liuyaohua.guava.concurrent;

//代理角色：

import org.junit.Test;

import java.lang.reflect.*;

/**
 * 作者 yaohua.liu
 * <p/>
 * 日期 2014-11-27 17:44
 * <p/>
 * * <p/>
 * 说明 动态代理
 * Java动态代理类位于Java.lang.reflect包下，一般主要涉及到以下两个类：
 * (1). Interface InvocationHandler：该接口中仅定义了一个方法Object：invoke(Object obj,Method method, Object[] args)。在实际使用时，第一个参数obj一般是指代理类，method是被代理的方法，如上例中的request()，args为该方法的参数数组。这个抽象方法在代理类中动态实现。
 * (2).Proxy：该类即为动态代理类，作用类似于上例中的ProxySubject，其中主要包含以下内容：Protected Proxy(InvocationHandler h)：构造函数，估计用于给内部的h赋值。
 * <p/>
 * Static Class getProxyClass (ClassLoader loader, Class[] interfaces)：获得一个代理类，其中loader是类装载器，interfaces是真实类所拥有的全部接口的数组。
 * Static Object newProxyInstance(ClassLoader loader, Class[] interfaces, InvocationHandler h)：返回代理类的一个实例，返回后的代理类可以当作被代理类使用(可使用被代理类的在Subject接口中声明过的方法)。
 * 所谓Dynamic Proxy是这样一种class：它是在运行时生成的class，在生成它时你必须提供一组interface给它，然后该class就宣称它实现了这些interface。你当然可以把该class的实例当作这些interface中的任何一个来用。
 * 当然啦，这个Dynamic Proxy其实就是一个Proxy，它不会替你作实质性的工作，在生成它的实例时你必须提供一个handler，由它接管实际的工作。
 *
 * 优点：
 * 1）主要用来做方法的增强，让你可以在不修改源码的情况下，增强一些方法，在方法执行前后做任何你想做的事情（甚至根本不去执行这个方法），因为在InvocationHandler的invoke方法中，
 * 你可以直接获取正在调用方法对应的Method对象，具体应用的话，比如可以添加调用日志，做事务控制等。

 2）还有一个有趣的作用是可以用作远程调用，比如现在有Java接口，这个接口的实现部署在其它服务器上，在编写客户端代码的时候，没办法直接调用接口方法，因为接口是不能直接生成对象的，
 这个时候就可以考虑代理模式（动态代理）了，通过Proxy.newProxyInstance代理一个该接口对应的InvocationHandler对象，然后在InvocationHandler的invoke方法内封装通讯细节就可以了。
 具体的应用，最经典的当然是Java标准库的RMI，其它比如hessian，各种webservice框架中的远程调用，大致都是这么实现的。
 */
public class _JDK_DynamicProxyTest {

    //抽象角色(之前是抽象类，此处应改为接口)：
    private  interface Subject{
        void request();
    }
    private  interface Subject2{
        void request();
    }

    //具体角色RealSubject：实现了Subject接口的request()方法。
    private class RealSubject implements Subject,Subject2{
        public RealSubject(){

        }
        public void request(){
            System.out.println("From real subject.");
        }
    }

    private class DynamicSubject implements InvocationHandler{
        private Object sub;
        public DynamicSubject(Object sub){
            this.sub = sub;
        }
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println("before calling " + method);
            method.invoke(sub,args);
            System.out.println("after calling " + method);
            return null;
        }
    }

    @Test
    public void test()
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        //RealSubject realSubject = new RealSubject(); //在这里指定被代理类
        InvocationHandler ds = new DynamicSubject(new RealSubject()); //初始化代理类
        //Class aClass = realSubject.getClass();

        //以下是分解步骤
        Class c = Proxy.getProxyClass(RealSubject.class.getClassLoader(), RealSubject.class.getInterfaces());
        Constructor ct = c.getConstructor(InvocationHandler.class);

        //Subject subject = (Subject) ct.newInstance(new Object[] { ds });
        Subject2 subject = (Subject2) ct.newInstance(ds);

        //以下是一次性生成
        //Subject subject = (Subject) Proxy.newProxyInstance(RealSubject.class.getClassLoader(), RealSubject.class.getInterfaces(), ds);
        subject.request();
    }
}
