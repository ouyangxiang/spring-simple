package com.example.liuyaohua.guava.concurrent;

import com.google.common.util.concurrent.Monitor;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-28 15:17
 * * 说明 Monitor就像java原生的synchronized, ReentrantLock一样，每次只允许一个线程执行代码块，且可重占用，每一次占用要对应一次退出占用。
 * 通过Monitor的Guard进行条件阻塞
 */
public class MonitorTest {

    @Test
    public void test() throws InterruptedException {
        Monitor monitor = new Monitor();
        // ========================================
        monitor.enter();
        try {
            // do things while occupying the monitor
            addToList("liu");
        } finally {
            monitor.leave();
        }
        // ========================================

        // -------------------------------------------------------
        if (monitor.tryEnter()) {
            try {
                // do things while occupying the monitor
                addToList("liu");
            } finally {
                monitor.leave();
            }
        } else {
            // do other things since the monitor was not available
        }
        // -------------------------------------------------------
    }

    private List<String> list = new ArrayList<String>();
    private static final int MAX_SIZE = 10;
    private Monitor monitor = new Monitor();

    private Monitor.Guard condition = new Monitor.Guard(monitor) {
        @Override
        public boolean isSatisfied() {
            return list.size() < MAX_SIZE;
        }
    };

    public void addToList(String item) throws InterruptedException {
        //Guard(形如Condition)，不满足则阻塞，而且我们并没有在Guard进行任何通知操作
        monitor.enterWhen(condition);
        try {
            list.add(item);
        } finally {
            monitor.leave();
        }
    }
}
