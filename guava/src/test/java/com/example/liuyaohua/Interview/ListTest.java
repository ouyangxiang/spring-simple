package com.example.liuyaohua.Interview;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * 作者： yaohua.liu
 * 日期： 2017-07-10
 * 描述： ...
 */
public class ListTest {

	@Test
	public void test(){
		ArrayList<java.lang.String>  arrayList = new ArrayList(10);
		arrayList.add("a");
		arrayList.add("b");
		arrayList.remove("a");
		arrayList.get(0);

		LinkedList<java.lang.String>  linkedList = new LinkedList();
		linkedList.add("a");
		linkedList.add("b");
		linkedList.remove("a");
		linkedList.get(0);
	}
}
