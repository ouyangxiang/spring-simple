package com.example.liuyaohua.guava.collect;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.junit.Test;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-21 17:43
 * * 说明 双向map 键与值都是唯一的，可以把键与值调换
 */
public class BiMapTest {
    @Test
    public void testBiMap(){

        // 反转key、value
        BiMap<Integer,String> biMap= HashBiMap.create();
        biMap.put(3, "Helium");
        Integer inverse=biMap.inverse().get("Helium");

        biMap.forcePut(3,"aa");
        String str=biMap.get(3);

        // put一个与原来相同的key或value都把把原来的覆盖
        biMap.put(3,"333");
        String result=biMap.get(3);
    }
}
