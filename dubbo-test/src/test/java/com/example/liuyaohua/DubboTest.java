package com.example.liuyaohua;

import com.example.liuyaohua.service.DubboService;
import org.junit.Assert;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * <p>作者 yaohua.liu
 * <p>日期 2018-07-24 18:27
 * <p>说明 ...
 */
public class DubboTest extends CommonTest {

    @Resource
    private DubboService dubboService;

    @Test
    public void testDubbo(){
        String hello = dubboService.sayHello(1);
        Assert.assertNotNull(hello);
        Assert.assertEquals(hello,"123456");

        hello = dubboService.sayHello(123);
        Assert.assertNotNull(hello);
        Assert.assertEquals(hello,"000000");
    }
}
