package com.example.liuyaohua.unit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

/**
 * @author yaohua.liu
 *         Date: 2015-5-13 Time: 下午12:12
 */
@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试
@ContextConfiguration({ "classpath*:spring/spring-config.xml" }) //加载配置文件
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class })
//------------如果加入以下代码，所有继承该类的测试类都会遵循该配置，也可以不加，在测试类的方法上/
//控制事务，参见下一个实例
//这个非常关键，如果不加入这个注解配置，事务控制就会完全失效！
//@Transactional
//这里的事务关联到配置文件中的事务控制器（transactionManager = "transactionManager"），同时
//指定自动回滚（defaultRollback = true）。这样做操作的数据才不会污染数据库！
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class CommonTest {
	public Logger logger = LoggerFactory.getLogger(this.getClass());

//	protected String phone = Configuration.getValue("phone_no", ChannelType.UTEST);
//	protected String userId = Configuration.getValue("user_id", ChannelType.UTEST);
//	protected String platformCode = Configuration.getValue("platform_code", ChannelType.UTEST);
//	protected String lng = Configuration.getValue("lng", ChannelType.UTEST);
//	protected String lat = Configuration.getValue("lat", ChannelType.UTEST);
//	protected String clientIdentify = Configuration.getValue("clientIdentify", ChannelType.UTEST);
//	//protected String name = Configuration.getValue("name", ChannelType.UTEST);// 流程状态为0
//	protected String name = "test";       // 状态 = 0
//	protected String name_null = "test_null";// 数据库中不存在该用户的流程状态
//	protected String name_20 = "test_20";// 状态 = 20 & 在额度预估有效期内
//	protected String name_50 = "test_50";// 状态 = 50
//	protected String name_60 = "test_60";// 状态 = 60 & 手机验证成功
//	protected String name_90 = "test_90";// 状态 = 90 &  在进件有效期内
//	protected String test_peroid = "test_peroid";// 状态 = 10 &  在进件有效期内
//
//	protected String system_model = Configuration.getValue("system_model", ChannelType.UTEST);
//	protected String system_phone = Configuration.getValue("system_phone", ChannelType.UTEST);
//	protected String password = "test";
//
//	protected BaseInfo baseInfo = new BaseInfo().setName(name).setUserId(userId).setPhoneNumber(phone).setPlatformCode(platformCode).setClientIdentify(clientIdentify).setLat(lat).setLng(lng).setSystemModel(system_model).setSystemPhone(system_phone);
//
//	protected String applyId = "111111111111111111111";
//	protected String passportId = "Y20161009000030076";
//	protected String channel = "ZC01020001";
//	protected String valueString = "40000.00";
//	protected float value = 40000.00f;

	@Test
	public void test() {
		Assert.assertTrue(true);
	}
}
