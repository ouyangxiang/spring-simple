package com.example.liuyaohua.Interview;

import org.junit.Test;
import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 作者 yaohua.liu
 * 日期 2014-11-27 17:14
 * 动态代理
 */
public class MyProxyTest2 {

	public interface Subject {
		void doSomething();
	}

	public class RealSubject implements Subject {
		public void doSomething() {
			System.out.println("call doSomething()");
		}
	}

	public class ProxyHandler implements InvocationHandler {
		private Object proxied;

		public ProxyHandler(Object proxied) {
			this.proxied = proxied;
		}

		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			//在转调具体目标对象之前，可以执行一些功能处理
			doSomeThing("before");

			//转调具体目标对象的方法
			Object obj = method.invoke(proxied, args);

			//在转调具体目标对象之后，可以执行一些功能处理
			doSomeThing("after");

			return obj;
		}

		private void doSomeThing(java.lang.String show) {
			System.out.println(show);
		}
	}

	@Test
	public void test() {

		RealSubject real = new RealSubject();
		Subject proxySubject = (Subject) Proxy.newProxyInstance(Subject.class.getClassLoader(),
				new Class[] { Subject.class },
				new ProxyHandler(real));

		proxySubject.doSomething();
//		before
//		call doSomething()
//		after

		//write proxySubject class binary data to file
		createProxyClassFile();
	}

	public static void createProxyClassFile() {
		java.lang.String name = "ProxySubject";
		byte[] data = ProxyGenerator.generateProxyClass(name, new Class[] { Subject.class });
		try {
			FileOutputStream out = new FileOutputStream(name + ".class");
			out.write(data);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
