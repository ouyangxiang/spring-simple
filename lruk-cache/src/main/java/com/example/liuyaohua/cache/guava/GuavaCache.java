package com.example.liuyaohua.cache.guava;

import com.google.common.collect.ImmutableMap;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 作者 yaohua.liu
 * 日期 2018-03-30 18:20
 * 说明 Guava缓存服务
 */
public interface GuavaCache<K, V> {

    /**
     * 根据key 获取value（方法本身不会抛异常，除非loadData方法抛异常）
     * <p>
     * 1、从本地缓存中取
     * 2、如果本地缓存中不存在则从loader补偿一次数据(手动)
     */
    V getFromCache(K key);

    /**
     * 获取多个值
     */
    Collection<V> getAllForList(Set<K> ks);

    /**
     * 获取多个值
     */
    Map<K, V> getAll(Set<K> ks);

    /**
     * 批量查询
     */
    ImmutableMap<K, V> getAll(Iterable<? extends K> keys) throws ExecutionException;

    /**
     * 添加key value
     *
     * @param key   K
     * @param value V
     */
    void put(K key, V value);

    /**
     * 当cache中没有值的时候使用该方法获取值
     */
    V loadData(K key);

    /**
     * 初始化缓存保存的最大数量
     */
    long initMaximumSize();

    /**
     * 默认的超时时间单元，默认为秒
     *
     * @return TimeUnit
     * @see TimeUnit#SECONDS
     */
    TimeUnit initTimeUnit();

    /**
     * 初始化缓存超时时长，默认为60秒
     */
    long initTimeOut();

    /**
     * 统计缓存中存在的K的数量
     */
    long size();

    /**
     * 根据key 获取value,如果value不存在则从loader中取
     * 注：当返回值为空时，抛 ExecutionException 异常
     */
    V get(K key) throws ExecutionException;

    /**
     * 根据key 获取value,如果value不存在则从loader中取（返回null值时不抛异常）
     * 注：不显式抛异常，但可能会抛 UncheckedExecutionException 异常
     */
    V getUnchecked(K key);
}
