package com.example.liuyaohua.util.excel;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者 yaohua.liu
 * 日期 2016-08-25 15:06
 * 说明 ...
 */
public class ExcelDataFormatter {
    private Map<String, Map<String, String>> formatter = new HashMap<String, Map<String, String>>();

    public void set(String key, Map<String, String> map) {
        formatter.put(key, map);
    }

    public Map<String, String> get(String key) {
        return formatter.get(key);
    }
}
