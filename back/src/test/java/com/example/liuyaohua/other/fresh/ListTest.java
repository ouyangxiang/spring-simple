package com.example.liuyaohua.other.fresh;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * 作者 yaohua.liu
 * 日期 2014-10-24 15:09
 * 包名 com.qunar.fresh
 * 说明 ...
 */
public class ListTest {

    public static void main(String[] args) {
        List<String> test= Lists.newArrayList();
        test.add("1");
        test.add("2");
        test.add("3");
        test.add("4");
        test.add("5");
        test.add("6");
        List<String> my=test.subList(2,5);
        System.out.println("1:"+test.size()+","+test.toString());

        System.out.println("2:"+my.size()+","+my.toString());

        my.remove(1);
        System.out.println("3:"+my.size()+","+my.toString());

        test.remove(1);

        System.out.println("4:"+test.size()+","+test.toString());

        System.out.println("5:"+my.size()+","+my.toString());
    }
}
