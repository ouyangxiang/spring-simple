package com.example.liuyaohua.core.redis;

import com.example.liuyaohua.util.Configuration;
import com.example.liuyaohua.util.Configuration;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.ArrayList;
import java.util.List;

public class JedisManager {

	private ShardedJedisPool shardPool;

	class JedisConfig {
		private JedisConfig() {
			initaize();
		}

		private void initaize() {
			System.out.println("redis initaizing ...");
			JedisPoolConfig pConfig = new JedisPoolConfig();
			pConfig.setMaxIdle(Configuration.getIntValue("redis.maxIdle"));
			pConfig.setMinIdle(Configuration.getIntValue("redis.minIdle"));
			pConfig.setTestOnBorrow(Boolean.valueOf(Configuration.getValue("redis.testOnBorrow")));
			int nodes =Configuration.getIntValue("redis.cluster.count");
			// 单位:毫秒
			Integer poolTimeout = Configuration.getIntValue("redis.pool.timeout");

			List<JedisShardInfo> jdinfoList = new ArrayList<JedisShardInfo>();
			for (int i = 0; i < nodes; i++) {

				String masterHost = Configuration.getValue("redis.cluster.master." + (i + 1) + ".host");
				String masterPort =Configuration.getValue("redis.cluster.master." + (i + 1) + ".port");

				JedisShardInfo jsi = new JedisShardInfo(masterHost, Integer.valueOf(masterPort), poolTimeout);
				jsi.setPassword(Configuration.getValue("redis.cluster.password"));
				jdinfoList.add(jsi);
			}

			shardPool = new ShardedJedisPool(pConfig, jdinfoList);
			System.out.println("redis initialize success");
		}
	}

	private JedisManager() {
		new JedisConfig();
	}

	private static JedisManager instance = new JedisManager();

	public static JedisManager getInstance() {
		return instance;
	}

	public ShardedJedis getShardedJedis() {
		return shardPool.getResource();
	}

	/**
	 * 正常归还jedis
	 * 
	 * @param jedis
	 */
	public void returnSharedJedis(final ShardedJedis jedis) {

		shardPool.returnResource(jedis);
	}

	/**
	 * jedis出现异常的时候销毁jedis
	 * 
	 * @param jedis
	 */
	public void returnBrokenSharedJedis(final ShardedJedis jedis) {
		shardPool.returnBrokenResource(jedis);
	}

	public static void main(String[] args) {
		JedisManager jedis = new JedisManager();
//		JedisHelper.del("supportCityList");
//		System.err.println("+++++"+JedisHelper.getList("supportCityList"));
		jedis.getShardedJedis().exists("haol");
		jedis.getShardedJedis().set("haol", "ttt");
		System.err.println(jedis.getShardedJedis().get("haol"));
	}
}
