package com.example.liuyaohua.cache.lruk;

import com.example.liuyaohua.cache.lruk.sync.SyncResult;
import com.google.common.collect.ImmutableMap;

/**
 * 作者 yaohua.liu
 * 日期 2018-03-30 18:20
 * 说明 lruk缓存服务
 */
public interface LrukCache<K, V> {
    /**
     * 根据key 获取value,如果value不存在则从loader中取
     */
    V get(K key) throws Exception;

    /**
     * 根据key 获取value,如果value不存在则从loader中取（返回null值时不抛异常）
     */
    V getUnchecked(K key);

    /**
     * 根据key 获取value,如果value不存在则从loader中取
     *
     * @param key            K
     * @param fromLocalCache 是否从本地缓存获取数据，true 是，false 不是(直接从load方法加载数据)
     */
    V get(K key, boolean fromLocalCache) throws Exception;

    /**
     * 批量查询
     */
    ImmutableMap<K, V> getAll(Iterable<? extends K> keys) throws Exception;

    /**
     * 添加key value
     *
     * @param key   K
     * @param value V
     */
    void put(K key, V value);

    /**
     * 是否包含key
     *
     * @param key K
     * @return boolean true：包含，false：不包含
     */
    boolean containsKey(K key);

    /**
     * 根据key删除对应的值
     */
    V remove(K key);

    /**
     * 把key对应的值替换成value：key为null 或 value为null时返回
     *
     * @return V 返回原来的对象
     */
    V replace(K key, V value);

    /**
     * 当cache中没有值的时候使用该方法获取值
     */
    V load(K key);

    /**
     * 重新加载key
     */
    V reload(K key);

    /**
     * 动态获取 K 阀值
     */
    int getK();

    /**
     * 初始化缓存保存的最大数量
     */
    long initMaximumSize();

    /**
     * 初始化缓存刷新时间(默认单位是秒)，可重写initTimeUnit()方法修改单位
     * k结构是根据上一次读到现在之间的时间是否超过当前值
     * 缓存是根据上一次写到现在之间的时间是否超过当前值
     */
    long initTimeOut();

    /**
     * 统计缓存中存在的K的数量
     */
    long size();

    /**
     * 向address(可能有多个url)中的url依次发送请求，统计返回数据
     *
     * @param key 需要同步的key
     * @return SyncResult
     */
    SyncResult syncLrukCache(K key);
}
