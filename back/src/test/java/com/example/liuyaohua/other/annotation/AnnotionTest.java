package com.example.liuyaohua.other.annotation;

import org.junit.Test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 作者 yaohua.liu
 * 日期 2015-06-03 16:30
 * 包名 annotation
 * 说明 ...
 */
public class AnnotionTest {

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    private @interface Hello {
        int code() default 1;
    }

    private class Operate{

        public void find(){
            System.out.println(-1);
        }

        /**
         * 如果没有传入code,default 1生效
         * @param code
         */
        @Hello
        public void findDefault(int code){
            System.out.println(code);
        }

        /**
         * 如果没有传入code，code = 2生效
         * @param code
         */
        @Hello(code = 2)
        public void findHelloCode(int code){
            System.out.println(code);
        }

        public int cc = 1;
    }

    /**
     * 注解 测试
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    @Test
    public void operateTest() throws InvocationTargetException, IllegalAccessException {
        Operate operate = new Operate();
        Method[] methods = operate.getClass().getDeclaredMethods();
        for (Method method : methods) {
            System.out.print(method.getName() + " : ");
            Hello hello = method.getAnnotation(Hello.class);
            if (hello == null) {
                method.invoke(operate);
            } else {
                method.invoke(operate, hello.code());
            }
        }
    }

    /**
     * 字段 测试
     * @throws IllegalAccessException
     */
    @Test
    public void fieldTest() throws IllegalAccessException {
        Operate operate = new Operate();
        Field[] fields = Operate.class.getFields();// 不能使用 getDeclaredFields 会报错
        for (Field field : fields) {
            if (field == null) {
                continue;
            }
            field.setAccessible(true);
            field.setInt(operate, -11);
            System.out.println(field.getType() + ": " + field.getName() + " = " + field.getInt(operate));
        }
    }
}
